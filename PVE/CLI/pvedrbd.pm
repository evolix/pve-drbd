package PVE::CLI::pvedrbd;

use strict;
use warnings;

use PVE::RPCEnvironment;
use PVE::CLIFormatter;
use PVE::CLIHandler;

use PVE::API2::Settings;
use PVE::API2::File;
use PVE::API2::Resource;
use PVE::API2::File::Node;
use PVE::API2::Utils::Migrate;
use base qw(PVE::CLIHandler);

sub setup_environment {
    PVE::RPCEnvironment->setup_default_cli_env();
}

my $upid_exit = sub {
    my $upid = shift;
    my $status = PVE::Tools::upid_read_status($upid);
    print "Task $status\n";
    exit(PVE::Tools::upid_status_is_error($status) ? -1 : 0);
};

my $print_cert_info = sub {
    my ($schema, $cert, $options) = @_;

    my $order = [qw(filename fingerprint subject issuer notbefore notafter public-key-type public-key-bits san)];
    PVE::CLIFormatter::print_api_result(
	$cert, $schema, $order, { %$options, noheader => 1, sort_key => 0 });
};


sub printCharLen {
	my ($char, $len) = @_;

	my $index = 0;
	while ($index < $len) {
		print($char);
		$index++;
	}
}

my $printStatus = sub {
	my ($ret) = shift;

	if ($ret->{status} != 1) {
		print $ret->{message}."\n";
        exit $ret->{status};
	}
	$ret->{message} = "OK." if (! (defined $ret->{message}) || $ret->{message} eq "");
	print($ret->{message}."\n");
};

my $printHashArray = sub {
	my ($ret) = shift;

	my @optArr = sort keys (%{$ret->[0]});

	exit 2 if (! (scalar @optArr));

	my @lenArr = ();
	for my $opt (@optArr) {
		my $maxlen = length($opt);
		for my $hash (@{$ret}) {
			$maxlen = length($hash->{$opt}) if (length($hash->{$opt}) > $maxlen);
		}
		my $splitlen = int(($maxlen - length($opt)) / 2);
		printCharLen(" ", $splitlen);
		print(uc $opt);
		printCharLen(" ", $splitlen);
		print(" ") if (($maxlen - length($opt)) % 2);
		print(" | ");
		push @lenArr, $maxlen;
	}
	print("\n");
	for my $len (@lenArr) {
		printCharLen("-", $len);
		print(" | ");	
	}
	print("\n");

	for my $hash (@{$ret}) {
		my $index = 0;
		for my $opt (@optArr) {
			print($hash->{$opt});
			printCharLen(" ", $lenArr[$index++] - length($hash->{$opt}));
			print(" | ");
		}
		print("\n");
	}

};



our $cmddef = {
	file => {
		create => ['PVE::API2::File', 'file_create', ["name", "res_name", "disk", "device", "port", "node"], {}, $printStatus ],
		delete => ['PVE::API2::File', 'file_delete', ["path"], {}, $printStatus ],
		get => ['PVE::API2::File', 'file', [], {}, $printHashArray ],
	},
	category => {
		add => ['PVE::API2::File', 'new_category', ["res_name", "category", "value"], {}, $printStatus ],
		remove => ['PVE::API2::File', 'del_category', ["res_name", "category", "value"], {}, $printStatus ],
		getall => ['PVE::API2::File', 'get_categories', ["res_name"], {}, $printHashArray ],
		get => ['PVE::API2::File', 'get_category', ["res_name", "category"], {}, $printHashArray ],
	},
	node => {
		add => ['PVE::API2::File::Node', 'create_lv_node', ["res_name", "node", "create"], {}, $printStatus ],
		remove => ['PVE::API2::File::Node', 'remove_node', ["res_name", "node", "delete"], {}, $printStatus ],
		get => ['PVE::API2::File::Node', 'get_nodes', ["res_name"], {}, $printHashArray ],
	},
    conf => {
		get => [ 'PVE::API2::Settings', 'get_settings', [], {}, $printHashArray ],
		set => [ 'PVE::API2::Settings', 'set_setting', ["option", "newvalue"], {}, $printStatus ],
    },
	get => ['PVE::API2::Resource', 'status', ["node"], {}, $printHashArray ],
	start => ['PVE::API2::Resource', 'start', ["res_name"], {}, $printStatus ],
	stop => ['PVE::API2::Resource', 'stop', ["res_name"], {}, $printStatus ],
	change => ['PVE::API2::Resource', 'status_change', ["res_name", "node", "status"], {}, $printStatus ],
	migrate => [ 'PVE::API2::Utils::Migrate', 'hot_migrate', ["vmid", "src_node", "dst_node", "create"], {}, $printStatus ],
	danse => [ 'PVE::API2::Utils::Migrate', 'drbd_danse', ["vmid", "first_node", "second_node", "create"], {}, $printStatus ],
};

1;
