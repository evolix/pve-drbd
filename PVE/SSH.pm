package PVE::SSH;

use PVE::Tools qw(run_command trim);
use PVE::INotify;

sub node_opt { #Used to add node name to the run_command's functions argument (ex: outfunc)
    my ($node, %opt) = @_;

    for my $key (keys %opt){
        if (ref($opt{$key}) eq 'CODE') {
            my $orig_func = $opt{$key};
            $opt{$key} = sub {
                $orig_func->($node, shift);
            };
        }
    }
    return %opt
};


sub run_command_nodes { #Run a shell command on all node by ssh
    my ($cmd, $nodes_ref, %opt) = @_;

    my $actual_host = PVE::INotify::nodename();

    for my $node (@{$nodes_ref}){
        my $exec_cmd = $cmd;
        $exec_cmd = "/usr/bin/ssh -o StrictHostKeyChecking=no $node ".$exec_cmd if (!($node eq $actual_host));
        run_command($exec_cmd, node_opt($node, %opt)); 
    }
}

1;