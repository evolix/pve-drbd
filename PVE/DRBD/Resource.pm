package PVE::DRBD::Resource;
use warnings;
use strict;

use PVE::DRBD::Utils;
use PVE::DRBD::Config::Parser;

# Resource hash format:
# resource resname => {
# 	data => [
# 		disk /dev/...
# 		device /dev/..
# 	]

# 	on node1 {
# 		data => [
# 			address 192.16....
# 		]
# 	}

# 	net {
# 		data => [
# 			allow-two-primaries...
# 		]
# 	}
# }

sub get_res_key { # Get res name not formated (on resname)
	my ($data) = @_;
	my ($key) = keys %{$data};

	return $key;
}

sub get_res_name { # Get real res name (resname)
		my ($data) = @_;

		my $res_line = get_res_key($data);
		my @splited_res = split(" ", $res_line);
		my $name = $splited_res[1];
		$name =~ s/^["']|["']$//g;
		return $name;
}

sub format_res {
	my ($name) = @_;
	return "resource $name";
}

sub format_node {
	my ($node) = @_;
	return 'on '.$node;
}

sub format_volume {
	my ($id) = @_;
	return 'volume '.$id;
}

sub clean_category { # Delete all empty category
	my ($conf_ref, $category) = @_;

	my %conf = %{$conf_ref};
	my $resource_key = get_res_key($conf_ref);

    if (!@{$conf{$resource_key}{$category}{'data'}}) {
        delete $conf{$resource_key}{$category}{'data'};
    }

    if (!%{$conf{$resource_key}{$category}}) {
        delete $conf{$resource_key}{$category};
    }
}

sub addBase { # Add to resname->data
	my ($conf_ref, $value) = @_;

	my %conf = %{$conf_ref};
    my $res = get_res_key($conf_ref);
	my $format_value = $value.";";

    for my $line ( @{$conf{$res}{'data'}}) {
        return 1 if ($line eq $format_value);
    }

    push @{$conf{$res}{'data'}}, $format_value;
    
    return 1;
}

sub delBase { # Del from resname->data

	my ($conf_ref, $value) = @_;

	my %conf = %{$conf_ref};

	my $res = get_res_key($conf_ref);

    if (exists $conf{$res}{'data'}) {
		my $format_value = $value.";";
        # Suppression des options qui correspondent à la valeur spécifiée
        @{$conf{$res}{'data'}} = grep { $_ ne $format_value } @{$conf{$res}{'data'}};
    }
	return 1;
}

sub getBase { # Get from resname->data
	my ($conf_ref, $value) = @_;

	my %conf = %{$conf_ref};
    for my $key (keys %conf) {
        my %res = %{$conf{$key}};
		return undef if (!$res{data});
        my @data = @{$res{data}};
        for my $line (@data) {
            if ($line =~ /^\Q$value\E\s+(.*)/) {
                my $device = $1;
                $device =~ s/;$//;
                return $device;
            }
        }
    }
	return undef;
}

sub updateConnectionMesh { # Auto update the node-id of all nodes, add one if not exist and update connection-mesh category
	my ($conf_ref) = @_;

	my %conf = %{$conf_ref};
	my $resname = PVE::DRBD::Resource::get_res_key($conf_ref);
	my $nodes = PVE::DRBD::Resource::getNodes($conf_ref);

	@{$conf{$resname}{"connection-mesh"}{"data"}} = ();

	my $previousid = -1;
	my @sort_array = ();
	my $nodeindex = 0;
	while ($nodeindex++ < scalar (keys %{$nodes})) {
		for my $node (keys %{$nodes}) {
			my $id_found = 0;
			for my $line (@{$conf{$resname}{$node}{"data"}}) { # Sort connection_mesh by node-id.
				if ($line =~ /node-id\s+(\d+);/) {
					my $id = PVE::DRBD::Utils::find_min_id($nodes, $previousid);
					$id_found = 1;
					if ($1 == $id && $id > $previousid) {
						push @sort_array, $node;
						$previousid = $id;
					}
				}
			}
			if (!$id_found) { # Create node-id on all nodes if don't exist
				my $id = PVE::DRBD::Utils::find_unused_id($nodes, 0);
				push @{$conf{$resname}{$node}{"data"}}, "node-id ".$id.";";
                return updateConnectionMesh($conf_ref);
			}
		}
	}
	my $connection_str = "hosts";
	for my $node (@sort_array) {
		my $name = (split(" ", $node, 0))[1];
		$connection_str = $connection_str." ".$name;
	}
	push @{$conf{$resname}{"connection-mesh"}{"data"}}, $connection_str.";";
	return $conf_ref;
}

sub addNode { # Add a "on nodename" with /etc/hosts search
	my ($conf_ref, $node, $port) = @_;

	my %conf = %{$conf_ref};
    my $resname = get_res_key($conf_ref);
    my $formatnode = format_node($node);
	my $ip = PVE::DRBD::Utils::getHostAddress($node);

	# PVE::DRBD::Utils::exit_error("Host $node isn't set in /etc/hosts file. See help for more informations.") if (!PVE::hostFile::doHostExist($node));
	return (2) if (!$ip);
	@{$conf{$resname}{$formatnode}{'data'}}  = ("address ".$ip.":$port;");

	updateConnectionMesh($conf_ref) #add node-id and connection-mesh if drbd9 
		if (PVE::DRBD::Utils::is_drbd_version("9"));
	return 1;

}

sub delNode {
	my ($conf_ref, $node) = @_;

	my %conf = %{$conf_ref};

	delete ($conf{get_res_key($conf_ref)}{format_node($node)});
	updateConnectionMesh($conf_ref) if (PVE::DRBD::Utils::is_drbd_version("9")); #add node-id and connection-mesh if drbd9 
	return 1;

}

sub getNodes {
	my ($conf_ref) = @_;

	my %conf = %{$conf_ref};
	my $data = {};
	my $res_name = get_res_key($conf_ref);
	for my $key (keys %{ $conf{$res_name} })
	{
		if ($key =~ /on /)
		{
			my @values = ();
			for my $line (@{$conf{$res_name}{$key}{data}}){
				push (@values, $line);
			}
			@{$data->{$key}} = @values;
		}
	}
	return $data;
}

sub addVolume { # Add separate volume (not used, remove ?)
	my ($conf_ref, @param) = @_;

	my %conf = %{$conf_ref};

	my $id = 0;

	for my $key (keys %{@conf{get_res_key($conf_ref)}}){
		if ((split(" ", $key))[0] eq "volume")
		{
			my $vol_id = (split(" ", $key))[1];
			$id = $vol_id + 1 if ($vol_id >= $id);
		}
	}

	delete $conf{get_res_key($conf_ref)}{format_volume($id)};
	for my $value (@param)
	{
		my $format_value = $value.";";
		push (@{$conf{get_res_key($conf_ref)}{format_volume($id)}{'data'}}, $format_value);
	}
	return 1;
}

sub delVolume {
	my ($conf_ref, $id) = @_;

	my %conf = %{$conf_ref};

	delete ($conf{get_res_key($conf_ref)}{format_volume($id)});
	return 1;

}

sub getVolumes {
	my ($conf_ref) = @_;

	my %conf = %{$conf_ref};
	my $data = {};


	for my $key (keys %{ $conf{get_res_key($conf_ref)} })
	{
		if ($key =~ /volume /)
		{
			my @values = ();
			for my $line (@{$conf{get_res_key($conf_ref)}{$key}{data}}){
				push (@values, $line);
			}
			@{$data->{$key}} = @values;
		}
	}
	return $data;
}

sub getPort { # Get resource port by looking at nodes in the file
	my ($conf_ref) = @_;

	my $nodes = PVE::DRBD::Resource::getNodes($conf_ref);

	my $ret = 0;
	for my $node (keys %{$nodes}){

        for my $opt (@{$nodes->{$node}}) {
            if ($opt =~ /^address/){
                my ($address, $port) = split(":", $opt);

                $port =~ s/;$//;
                die "Can't get resource port, there is multiple port used, please choose one." if ($ret && $ret != $port);
                $ret = $port;
            }
        }
	}
	return $ret;
}

sub addToCategory { # For all others categories
	my ($conf_ref, $category, $value) = @_;

	my %conf = %{$conf_ref};

    my $res = get_res_key($conf_ref);
	my $format_value = $value.";";

    for my $line (@{$conf{$res}{$category}{'data'}}) {
        return 2 if ($line eq $format_value);
    }
	if (!defined($value) || $value eq "")
	{
		@{$conf{$res}{$category}{'data'}} = ();
		return 3;
	}
    push @{$conf{$res}{$category}{'data'}}, $format_value;
    
    return 1;
}

sub delFromCategory {
	my ($conf_ref, $category, $value) = @_;

	my %conf = %{$conf_ref};
	my $format_value = $value.";";
    if (exists $conf{get_res_key($conf_ref)}{$category}{'data'}) {
        # Suppression des options qui correspondent à la valeur spécifiée
        @{$conf{get_res_key($conf_ref)}{$category}{'data'}} = grep { $_ ne $format_value} @{$conf{get_res_key($conf_ref)}{$category}{'data'}};
    }
	clean_category(\%conf, $category);
	return 1;

}

sub getCategory {
	my ($conf_ref, $category) = @_;

	my %conf = %{$conf_ref};
	my $res = get_res_key($conf_ref);

	my @data = ();

	if ($category eq "data") {
		if (exists $conf{$res}{'data'}) {
			@data = @{$conf{$res}{'data'}};
		}
	} else {
		if (exists $conf{$res}{$category} && exists $conf{$res}{$category}{'data'}) {
			@data = @{$conf{$res}{$category}{'data'}};
		}
	}

	my $ret = {};

	for my $option (@data)
	{
		$option =~ s/;$//;
		$ret->{$option} = "";
	}
	return $ret;
}


sub delValue {
	my ($conf_ref, $category, @value) = @_;
	my %ca= (
		node => \&delNode,
		volumes => \&delVolume,
		data => \&delBase,
	);
	return delFromCategory($conf_ref, $category, @value) if (!exists $ca{$category});
	return $ca{$category}->($conf_ref, @value);
}

sub addValue {
	my ($conf_ref, $category, @value) = @_;
	my %ca= (
		node => \&addNode,
		volumes => \&addVolume,
		data => \&addBase,
	);
	
	return addToCategory($conf_ref, $category, @value) if (!exists $ca{$category});

	return $ca{$category}->($conf_ref, @value);
}

sub getValues {
	my ($conf_ref, $category) = @_;
	my %ca= (
		node => \&getNodes,
		volume => \&getVolumes,
		port => \&getPort,
	);
	return getCategory($conf_ref, $category) if (!exists $ca{$category});
	return $ca{$category}->($conf_ref);
}

sub createfile {
	my ($path, $resname) = @_;

	my $fd;
    open($fd, '>', $path) or die "Impossible d'ouvrir le fichier '$path' en écriture : $!";
    print($fd "resource $resname {\n}\n");
    close($fd);
}


sub loadConf {
	my ($confFile) = @_;

	return PVE::DRBD::Config::Parser::parse_drbdfile($confFile);
}

sub saveConf {
	my ($confFile, $data, %opt) = @_;

	PVE::DRBD::Config::Parser::save_to_file($data, $confFile);
}
sub showConf {
	my ($confFile) = @_;

	printHach(loadConf($confFile));
}

sub printHach {
	my (%data) = @_;

	for my $key (keys(%data)){
		print("category: $key\n");
		for my $value (@{$data{$key}}){
			print("\t$value\n");
		}
	}
}

1;