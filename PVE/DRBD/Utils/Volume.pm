package PVE::DRBD::Utils::Volume;

use warnings;
use strict;

use PVE::SSH;
use PVE::DRBD::Resource;
use PVE::DRBD::Utils;

sub delete_lv_node {
    my ($path, $node) = @_;

    $ENV{PATH} = '/usr/sbin:/sbin:/bin:/usr/bin';

    my $cfg = PVE::DRBD::Resource::loadConf($path);
    my $disk = PVE::DRBD::Resource::getBase($cfg, "disk");
    my $lvinfo = PVE::DRBD::Utils::format_disk($disk);
    
    PVE::SSH::run_command_nodes("/usr/sbin/lvremove ".$lvinfo->{vg}."/".$lvinfo->{lv}." -y", [$node]);
}

sub create_lv_node { # Create logical volume on node by looking on all cluster nodes for a same-named lv to copy properties (size...), and create drbd meta-data if specified
    my ($path, $node) = @_;

    $ENV{PATH} = '/usr/sbin:/sbin:/bin:/usr/bin';

    my $cfg = PVE::DRBD::Resource::loadConf($path);

    my $resname = PVE::DRBD::Resource::get_res_name($cfg);
    my $disk = PVE::DRBD::Resource::getBase($cfg, "disk");
    my $device = PVE::DRBD::Resource::getBase($cfg, "device");

    my $errmsg = "";
    eval { PVE::SSH::run_command_nodes("lvdisplay | grep $disk", [$node]); };
    if ($@)
    {
        my $diskinfo = PVE::DRBD::Utils::format_disk($disk);
        my $lvinfo = PVE::DRBD::Utils::get_lv_info($diskinfo->{lv}, $diskinfo->{vg});
        die "No LV named $disk was found on cluster nodes. The auto LV creator needs to have a same-named LV on one cluster node to copy its properties and create it on the destination node." if ($lvinfo->{name} eq "");

        eval { PVE::SSH::run_command_nodes("/usr/sbin/lvcreate -L".$lvinfo->{size}." -n ".$lvinfo->{name}." ".$lvinfo->{vg}." -y", [$node], errfunc => sub { $errmsg .= $_[1]."\n"; }); };
        die "Can't create LV ".$lvinfo->{name}." on ".$node.": $errmsg" if ($errmsg ne "" && $@);
    }
    my ($filename) = $path =~ /\/([^\/]+)$/;

    eval { PVE::SSH::run_command_nodes("drbdadm status $resname", [$node]); };
    if ($@) {
        PVE::SSH::run_command_nodes("/usr/bin/cp $path /etc/drbd.d/$filename", [$node]);
        eval { PVE::SSH::run_command_nodes("/usr/sbin/drbdadm create-md $resname --force", [$node], errfunc => sub { $errmsg .= $_[1]."\n"; }); };
        die "Can't init DRBD meta-data on ".$node.". The /etc/drbd.d/$filename resource file isn't delete, try creating meta-data with it or delete the file. $errmsg" if ($@ && $errmsg ne "");
    }
    PVE::SSH::run_command_nodes("/usr/bin/rm /etc/drbd.d/$filename", [$node]);
}


1;