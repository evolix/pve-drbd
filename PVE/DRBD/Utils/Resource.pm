package PVE::DRBD::Utils::Resource;

use warnings;
use strict;

use PVE::DRBD::Resource;
use PVE::SSH;


sub start { # Start a resource on all nodes specified in it with ssh
    my ($path, $resname, $restrict_nodes) = @_;

        my $cfg = PVE::DRBD::Resource::loadConf($path);
        my $nodes = PVE::DRBD::Resource::getValues($cfg, "node");
        my $hosts = PVE::DRBD::Utils::get_nodes_name($nodes);
        my $disk = PVE::DRBD::Resource::getBase($cfg, "disk");
       
        $hosts = $restrict_nodes if ($restrict_nodes && scalar @{$restrict_nodes});

        my $errmsg = "";
        eval { PVE::SSH::run_command_nodes("lvdisplay | grep $disk", $hosts, outfunc => sub {}, errfunc => sub { $errmsg .= $_[1]."\n"; }); };
        return {status => 2, message => "[ERROR] Can't find disk $disk on all nodes: $errmsg"} if ($@);

        eval { PVE::SSH::run_command_nodes("cp $path /etc/drbd.d", $hosts); };

        eval { PVE::SSH::run_command_nodes("/sbin/drbdadm adjust $resname", $hosts, errfunc => sub { $errmsg .= $_[1]."\n"; }); };
        return {status => 4, message => "[ERROR] Can't launch DRBD: $errmsg"} if ($errmsg ne "" && $@);

        return {status => 1, message => ""};
}

sub stop { # Stop a resource on all nodes specified in it with ssh
    my ($path, $resname, $restrict_nodes) = @_;
    my ($filename) = $path =~ /\/([^\/]+)$/;


    my $cfg = PVE::DRBD::Resource::loadConf($path);

    my $device = PVE::DRBD::Resource::getBase($cfg, "device");
    my $nodes = PVE::DRBD::Resource::getValues($cfg, "node");

    my $hosts = PVE::DRBD::Utils::get_nodes_name($nodes);
    $hosts = $restrict_nodes if (scalar @{$restrict_nodes});

    my $errmsg = "";

    eval { PVE::SSH::run_command_nodes("/usr/sbin/qm list | awk 'NR > 1 {print \$1, \$2, \$3}'", $hosts, errfunc => sub { $errmsg .= $_[1]."\n"; });};
    return {status => 2, message => $errmsg} if ($errmsg ne "" && $@);

    eval { PVE::SSH::run_command_nodes("/usr/sbin/drbdadm down $resname", $hosts, errfunc => sub { $errmsg .= $_[1]."\n"; }); };
    return {status => 3, message => "Failed trying to stop resource '$resname': $errmsg"} if ($errmsg ne "" && $@);

    PVE::SSH::run_command_nodes("rm /etc/drbd.d/$filename", $hosts);

    return {status => 1, message => ""};    
}

sub status {
    my ($resname, $node, $status) = @_;

    my $errmsg = "";
    eval { PVE::SSH::run_command_nodes("/usr/sbin/drbdadm $status $resname", [$node], errfunc => sub { $errmsg .= $_[1]."\n"; })};
    return {status => 2, message => "[ERROR] $node: $errmsg"} if ($errmsg ne "" && $@);
    return {status => 1, message => ""};
}
1;