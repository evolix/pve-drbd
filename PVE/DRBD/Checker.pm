package PVE::DRBD::Checker;

use warnings;
use strict;
use PVE::DRBD::Resource;

#Checker for drbd resources

sub check_device {
    my $device = shift;

    return {status => 2, message => "The device '$device' is invalid."} if (! ($device =~ m{^/dev/drbd\d+$}));
    return {status => 1, message => ""};
}

sub check_port {
    my $port = shift;

    return {status => 2, message => "The port '$port' is invalid."} if (! ($port =~ /^\d+$/ && $port >= 1 && $port <= 65535));
    return {status => 1, message => ""};
}

sub check_disk {
    my $disk = shift;

    return {status => 2, message => "The disk '$disk' is invalid."} if (! ($disk =~ /^\/dev\//));
    return {status => 1, message => ""};
}

sub check_resource_dir {
    my $dir = shift;

    return {status => 2, message => "Path '$dir' is invalid. It must not finish with '/'."} if ($dir =~ /\/$/);
    return {status => 2, message => "Directory '$dir' do not exist."} if (! (-d $dir));
    return {status => 1, message => ""};
}

sub check_address {
    my $address = shift;

    return {status => 2, message => "The address '$address' is invalid."} if (! ($address =~ /^((25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{1,2})\.){3}(25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{1,2}):([0-9]{1,5})$/));

    return {status => 1, message => ""};
}



sub check_opt {
    my $opt = shift;

    my %checks = (
        device => \&check_device,
        port => \&check_port,
        disk => \&check_disk,
        address => \&check_address,
        "Resources directory" => \&check_resource_dir,
    );

    for my $tocheck (keys %{$opt}) {
        next if (! (exists $checks{$tocheck}));
        my $ret = $checks{$tocheck}->($opt->{$tocheck}); 
        return $ret if ($ret->{status} != 1);
    }
    return {status => 1, message => ""};
}

sub check_data {
    my $data_ref = shift;

    my %opt;

    for my $line (@{$data_ref}) {
        next if ($line eq "");

        my ($key, $value) = split(" ", $line, 2);
        $value = $key
            if (!$value);
        $value =~ s/;$//;
        $opt{$key} = $value;
    }
    return check_opt(\%opt);
}

sub check_cfg {
    my $hash = shift;

    for my $opt (keys %{$hash}) {
        my $err = {};
        if (ref($hash->{$opt}) eq 'HASH' )
        {
            for my $hash_values (keys %{$hash->{$opt}}) {
                if ($hash_values eq "data") {
                    $err = check_data($hash->{$opt}->{"data"})
                } else {
                    $err = check_cfg($hash->{$opt}->{$hash_values})
                }
                return $err if ($err->{status} != 1);
            }
        } else {
            $err = check_data($hash->{$opt});
        }
        return $err if ($err->{status} != 1);
    }
    return {status => 1, message => ""};
}

sub _checkConfV8 {
	my ($cfg) = @_;

	my $nodes = PVE::DRBD::Resource::getNodes($cfg);
	my $nb_node = keys %{$nodes};

	return {status => 2, messahe => "Too many node for drbd 8, upgrade to 9 to use multi-node"} if ($nb_node > 2);

    return check_cfg($cfg);
}

sub _checkConfV9 {
	my ($cfg) = @_;

    return check_cfg($cfg);
}


sub check_resource {
	my ($cfg) = @_;

	my $pve_version = PVE::DRBD::Utils::get_drbd_version();
	my $ver = (split(/\./, $pve_version))[0];
	return _checkConfV8($cfg) if ($ver eq "8");
	return _checkConfV9($cfg) if ($ver eq "9");
    return {status => 2, message => "$pve_version version of drbd isn't supported. Please considers to upgrade."};
}
1;