package PVE::DRBD::Utils;

use warnings;
use strict;

use PVE::SSH;
use PVE::Tools qw(run_command trim);
use PVE::Storage;
use PVE::Storage::Plugin;
use Storable qw(dclone);
use PVE::DRBD::Config;
use PVE::IPCC;
use JSON;
use PVE::QemuConfig;

sub openFile {
	my ($file, $type) = @_;
	my $fd;
	if (!open($fd, $type, $file)){
		print ("failed to open $file");
		exit(1);
	}
	return $fd;
}

sub exit_error {
	my ($error) = @_;
	print("$error\n");
	exit(1);
}

sub get_drbd_version { # Get drbd kernel module version
	my $drbd_kernel_version = 0;
    
    run_command('modinfo drbd | grep ^version: | awk \'{print $2}\'', outfunc => sub {
        $drbd_kernel_version = shift;
        $drbd_kernel_version = trim($drbd_kernel_version);
    });
	return $drbd_kernel_version;
}

sub is_drbd_version { # Check drbd kernel module version
    my $check_version = shift;
	my $pve_version = get_drbd_version();
	my $version = (split(/\./, $pve_version))[0];

    return 1 if $version eq $check_version;
    return 0;
}


sub get_storage_config { # Get pve storage config (opt in /etc/pve/storage.cfg)
    my ($storeid) = @_;

    my $cfg = PVE::Storage::config();

    my $scfg = dclone(PVE::Storage::storage_config($cfg, $storeid));
    $scfg->{storage} = $storeid;
    $scfg->{digest} = $cfg->{digest};
    $scfg->{content} = PVE::Storage::Plugin->encode_value($scfg->{type}, 'content', $scfg->{content});

    if ($scfg->{nodes}) {
        $scfg->{nodes} = PVE::Storage::Plugin->encode_value($scfg->{type}, 'nodes', $scfg->{nodes});
    }
    return $scfg;
};

sub get_vm_list {
	my $raw = PVE::IPCC::ipcc_send_rec(3);
    my $infos = decode_json($raw);

    my $vm_lists = $infos->{ids};
	return $vm_lists;
}

sub get_vm_storage_raw { # Get all storage of a specific VM, not formated
 my ($vmid) = @_;

	my $ret = ();
    my $vm_list = get_vm_list();
    my $conf = PVE::QemuConfig->load_config($vmid, $vm_list->{$vmid}->{node});
    for my $opt (keys %{$conf}){
        push @$ret, $conf->{$opt} if ($opt =~ /^(ide|scsi|virtio|sata|nvme)[0-9]/);
    }
	return $ret;
}

sub get_vm_storage_formated { # Get all storage of a specific VM, formated
	my ($vmid) = @_;

	my $ret = {};

    my $storages_raw = get_vm_storage_raw($vmid);

    for my $sto_raw (@$storages_raw){
        my ($sto, $rest) = split(":", $sto_raw);
        next if (!$rest);
        my @infos = split(",", $rest);
	
        for my $info (@infos){
            if ($info =~ /^\w+-\d+-\w+-\d+$/) {#Check if is name (xxx-vmid-disk-nb)
                $ret->{$sto}->{name} = $info;
            } elsif ($info =~ /^(.*?)=(.*)$/) {
               $ret->{$sto}->{$1} = $2;
            }
        }
    }
	return $ret;
}

sub get_all_nodes { # Get all cluster nodes.
    my @nodes;

    my $cmd = "/usr/bin/pvecm nodes | awk 'NR > 4 {print \$3}'";
    run_command($cmd, outfunc => sub {
        my $line = shift;

        $line = trim($line);
        push @nodes, $line;
    });

    return @nodes;
}

sub get_lv_list {
    my (@nodes) = @_;

    my $ret = {};

    @nodes = get_all_nodes() if (scalar @nodes == 0);
    
    my $res;
    PVE::SSH::run_command_nodes("/usr/sbin/lvs --noheadings -o lv_name,vg_name,lv_path,lv_size", \@nodes, 
    outfunc => sub {
        my ($node, $line) = @_;

        $line = trim($line);
        push @{$res->{$node}}, $line;
    }, noerr=> 1);


    for my $node (keys %{$res}) {
        for my $line (@{$res->{$node}}) {

            my ($name, $vg, $path, $size) = split(" ", $line);
            push @{$ret->{$node}}, {
                name => $name,
                vg => $vg,
                path => $path,
                size => $size,
            };
        }
    }
    return $ret;
}

sub get_lv_info { # Get information about a LV, by checking on all cluster node
    my ($lvname, $vg, $restrict_node) = @_;

    my $lvlist = get_lv_list();

    my $lvinfo = {
        name => "",
        vg => "",
        permissions => "",
        size => "",
    };

    for my $node (keys %{$lvlist}) {
        for my $lv (@{$lvlist->{$node}}) {
            if ($lv->{name} eq $lvname) {
                next if ($vg && $lv->{vg} ne $vg);
                die "Found multiple ".$lv->{name}." with different VG. Please specify a VG, rename other LV, or create LV and drbd meta-data manually " if ($lvinfo->{name} eq $lv->{name} && $lvinfo->{vg} ne $lv->{vg});
                die "Found multiple ".$lv->{name}." with same VG but not in size. Adapt LV size on all nodes, or create LV and drbd meta-data manually" if ($lvinfo->{name} eq $lv->{name} && $lvinfo->{size} ne $lv->{size});
                $lvinfo = $lv;
            }
        }
    }
    return $lvinfo;
}

sub getHostAddress {
	my ($name) = @_;

	my $fd = openFile("/etc/hosts", '<');

	my @lines = <$fd>;
	close ($fd);
	for my $line (@lines){
		$line =~ s/^\s+|\s+$//g;
		if ($line =~ /(?:^|\s)[^\s\w]*$name[^\s\w]*(?:\s|$)/ ){
			my @tab = split /[ \t]+/, $line;
			return $tab[0];
		}

	}
	return 0;
}


sub format_disk {
    my $disk = shift;

    my @infos = split("/", $disk);

    my ($lv, $vg) = (reverse @infos)[0, 1];

    my $diskinfo = {
        lv => $lv // "",
        vg => $vg // "",
    };

    return $diskinfo;
}

sub array_to_string {
    my (@arr) = @_;

    my $string = "";
    for my $storage (@arr){
        $string .= ", " if ($string ne "");
        $string .= $storage;
    }

    return $string;
}

sub get_nodes_name {
    my ($cfg) = @_;

    my $hosts = ();
    for my $keys (keys %{$cfg}){
        my $host = (split(" ", $keys))[1];
        $host = $1 if ($host =~ /^([a-zA-Z0-9._-]+)$/);
        push @$hosts, $host;
    }

    return $hosts;
}

sub get_path_res_name { # Get resource name with a path
    my ($res_path) = @_;

    my $conf = PVE::DRBD::Resource::loadConf($res_path);
    my $name = PVE::DRBD::Resource::get_res_name($conf);
    return $name;
}

sub drbd_status8 { # Get formated output of drbdadm status for drbd8, used by Proxmox UI for example
    my ($node) = @_;

    my $ret = {};
    my $actualkey;
    my $actual_res = "";
    PVE::SSH::run_command_nodes("/usr/sbin/drbdadm status", [$node], outfunc => sub {
        my ($node, $line) = @_;
        
        $line = trim($line);

        $actual_res = ""
            if ($line eq "");

        my @allinfos = split(" ", $line);
        foreach my $info (@allinfos) {

            if ($actual_res eq "") {
                $actual_res = $info;

                my $peer_name;
                my $cfg = get_cfg_from_name($actual_res);    
                my $nodes = PVE::DRBD::Resource::getValues($cfg->{cfg}, "node");
                my $hosts = get_nodes_name($nodes);
                my $actual_host = PVE::INotify::nodename();
                foreach my $host (@$hosts) {
                    $peer_name = $host if ($host ne $actual_host);
                }
    
                push @{$ret->{$actual_res}->{"peers"}}, { "name" => $peer_name };
            }

            next if (! ($info =~ /:/));

            my ($key, $value) = split(":", $info, 2);
            
            if ($key eq "role") {
                if (exists $ret->{$actual_res}->{"role"}) {
                    $ret->{$actual_res}->{"peers"}[0]->{"role"} = $value;
                    $ret->{$actual_res}->{"peers"}[0]->{"connection"} = "Connected"
                        if ($value eq "Primary" || $value eq "Secondary");
                } else {
                    $ret->{$actual_res}->{"role"} = $value;
                }
            }
            $ret->{$actual_res}->{"disk"} = $value
                if ($key eq "disk");
            $ret->{$actual_res}->{"peers"}[0]->{"replication"} = $value
                if ($key eq "replication");
            $ret->{$actual_res}->{"peers"}[0]->{"peer_disk"} = $value
                if ($key eq "peer-disk");
            $ret->{$actual_res}->{"peers"}[0]->{"percent"} = $value
                if ($key eq "done");
        }       
    }, noerr => 1);

    return $ret;
}

sub drbd_status9 { # Get formated output of drbdadm status for drbd9, used by Proxmox UI for example
    my ($node) = @_;

    my $ret = {};
    my $actualkey;

    my $all_lines = "";
    
    PVE::SSH::run_command_nodes("/usr/sbin/drbdsetup status --json", [$node], outfunc => sub {
        my ($node, $line) = @_;
        $all_lines = $all_lines.$line;
    });

    my $infos = decode_json($all_lines);

    for my $resource (@$infos) {
        my $resname = $resource->{name};

        $ret->{$resname}->{"role"} = $resource->{role};

        for my $device (@{$resource->{devices}}) {
            $ret->{$resname}->{"disk"} =  $ret->{$resname}->{"disk"}.", " if (exists $ret->{$resname}->{"disk"});
            $ret->{$resname}->{"disk"} = $device->{"disk-state"};
        }

        for my $peer (@{$resource->{connections}}) {
            my $res = {role => $peer->{"peer-role"}};

            $res->{"connection"} = $peer->{"connection-state"};
            $res->{"name"} = $peer->{"name"};

            for my $peer_devices (@{$peer->{"peer_devices"}}) {
                $res->{"peer_disk"} =  $res->{"peer_disk"}.", " if (exists $res->{"peer_disk"});
                $res->{"peer_disk"} = $peer_devices->{"peer-disk-state"};

                $res->{"replication"} =  $res->{"replication"}.", " if (exists $res->{"replication"});
                $res->{"replication"} = $peer_devices->{"replication-state"};
                $res->{"percent"} = $peer_devices->{"percent-in-sync"};

            }
            push @{$ret->{$resname}->{"peers"}}, $res;
        }
    }

    return $ret;
}

sub parse_drbd_status {
    my ($param) = @_;
    return drbd_status8($param) if (is_drbd_version("8"));
    return drbd_status9($param) if (is_drbd_version("9"));
}

sub get_res_primary { # Get hostname of primary node of a resource
    my ($path) = @_;

    my $ret = {};
        
    my $cfg = PVE::DRBD::Resource::loadConf($path);
    my $resname = get_path_res_name($path);

    my $node_cat = PVE::DRBD::Resource::getNodes($cfg);
    my $nodes = get_nodes_name($node_cat);
    
    die "Can't find primary node resource ".$resname.": No nodes in file." if (scalar @$nodes == 0);

    my $formated = parse_drbd_status($nodes->[0]);
    my $primary_nodes;
    for my $res (keys %{$formated}) {
            next if ($res ne $resname);

            push @$primary_nodes, $nodes->[0] if ($formated->{$res}->{role} eq "Primary");
        
            for my $peer (@{$formated->{$res}->{"peers"}}) {
                next if ($peer->{role} ne "Primary");
                my $name = $peer->{name};
                if (!($name) || $name eq "peer") {
                    for my $resnode (@$nodes) {
                        $name = $resnode if ($resnode ne $nodes->[0]);
                    }
                }
                push @$primary_nodes, $name if ($name ne $resname);
            }
    }
    return $primary_nodes;
}

sub convert_to_bytes {
    my ($size_str) = @_;
    
    my %units = (
        'k' => 1024,
        'm' => 1024 * 1024,
        'g' => 1024 * 1024 * 1024,
        't' => 1024 * 1024 * 1024 * 1024,
        'p' => 1024 * 1024 * 1024 * 1024 * 1024,
    );

    my ($size, $unit) = $size_str =~ /^([\d.]+)([kmgtpe]?)/i;
    $unit = lc($unit);
    return $size * $units{$unit};
}

sub find_unused_id {
    my ($nodes_ref, $actual_id) = @_;;

    for my $node (%{$nodes_ref}) {

        for my $line (@{$nodes_ref->{$node}}) {
            if ($line =~ /node-id\s+(\d+);/) {
                return find_unused_id($nodes_ref, $actual_id + 1) if ($1 == $actual_id);
            }
        }
    }
    return $actual_id;
}
sub find_min_id {
	my ($nodes_ref, $start) = @_;

	my $id = -1;
	for my $node (keys %{$nodes_ref}) {
		for my $line (@{$nodes_ref->{$node}}) {
			if ($line =~ /node-id\s+(\d+);/) {
				$id = $1 if ($1 > $start && ($1 < $id || $id == -1 ));
			}
		}
	}
	return $id;
}

sub get_cfg_from_name { # Get infos and config of a resource
    my $resname = shift;

    my $cfg = PVE::DRBD::Config::read_conf_file();

    for my $res (@{$cfg->{"Resources"}}) {
        my ($path, $port) = split(":", $res);

        my $conf = PVE::DRBD::Resource::loadConf($path);
        my $name = PVE::DRBD::Resource::get_res_name($conf);

        next if ($name ne $resname);
        return {cfg => $conf, name => $name, path => $path, port => $port};
    }
    return undef;
}


sub get_files_from_dir {
    my ($dir) = @_;

    my @files;

    opendir(my $dh, $dir) or die "Cannot open directory $dir: $!";
    
    @files = readdir($dh);
    
    closedir($dh);

    return @files;
}

1;
