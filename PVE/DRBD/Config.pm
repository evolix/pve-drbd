package PVE::DRBD::Config;
use warnings;
use strict;

use PVE::DRBD::Utils;
use PVE::Tools qw(run_command trim);

# Manipulate conf.cfg file


sub get_conf_dir {
    return "/etc/pve/pve-drbd";
}

sub get_conf_path {
    my $dir = get_conf_dir();
    
    return $dir."/conf.cfg";
}

my $path = get_conf_path();

sub get_setting_value {
    my ($opt) = @_;
    my @conf = PVE::DRBD::Config::get_values_for_category("Settings");

    for my $setting (@conf){
        my ($key, $value) = split(":", $setting);
        
        $key = trim($key);
        $value = trim($value);
        return $value if ($key eq $opt);
    }
}

sub conf_exist {
	return 1 if (-e $path);
	return 0;
}

sub createConf {
	my $writeFd = PVE::DRBD::Utils::openFile($path, '>');

	print($writeFd "Resources:\n");
	my @res_files = find_res_files(get_conf_dir());
	for my $file (@res_files){
        my $conf = PVE::DRBD::Resource::loadConf($file);
        my $port = PVE::DRBD::Resource::getValues($conf, "port");
        print($writeFd "\t$file:$port\n") if ($conf && $port);
	}

	print($writeFd "Settings:\n");
    my $version = PVE::DRBD::Utils::get_drbd_version();
    my $res_path = get_conf_dir();
    print($writeFd "\tVersion:$version\n\tResources directory:$res_path\n");
	close ($writeFd);


}

sub read_conf_file {
    my %data;
    my $current_category;

    open my $fh, '<', $path or die "Could not open file '$path': $!";

    while (my $line = <$fh>) {
        chomp $line;
        if ($line =~ /^(\w+):$/) {  # Match category lines
            $current_category = $1;
            $data{$current_category} = [];
        } elsif ($line =~ /^\t(.+)$/) {  # Match value lines
            push @{ $data{$current_category} }, $1 if defined $current_category;
        }
    }

    close $fh;
    return \%data;
}

sub add_value_to_category {
    my ($category, $value) = @_;

	my $data_ref = read_conf_file();
	$data_ref->{$category} = [] if (!defined $category);
    push @{ $data_ref->{$category} }, $value;
	write_conf_file($data_ref);
}

sub del_value_from_category {
    my ($category, $value) = @_;

	my $data_ref = read_conf_file();
	return 1 if (!defined $category || !defined $data_ref->{$category});
    my $index = 0;
    for my $line (@{ $data_ref->{$category} }) {
        if ($line =~ /$value/) {
            splice @{ $data_ref->{$category} }, $index, 1;
            $index--; # Décrémenter l'index car l'élément courant a été supprimé
        }
        $index++;
    }
	write_conf_file($data_ref);
    return 0;
}

sub get_values_for_category {
    my ($category) = @_;

	my $data_ref = read_conf_file();
    return unless exists $data_ref->{$category};
    return @{$data_ref->{$category}};
}

sub write_conf_file {
    my ($data_ref) = @_;

    open my $fh, '>', $path or die "Could not open file '$path': $!";

    for my $category (keys %$data_ref) {
        print $fh "$category:\n";
        for my $value (@{ $data_ref->{$category} }) {
            print $fh "\t$value\n";
        }
    }

    close $fh;
}

sub find_res_files {
    my ($directory) = @_;
    my @res_files;

    sub find_res_files_recursive {
        my ($dir, $files_ref) = @_;
        opendir my $dh, $dir or die "Cannot open directory $dir: $!";
        my @entries = readdir $dh;
        closedir $dh;

        for my $entry (@entries) {
            next if $entry eq '.' or $entry eq '..';
            my $path = "$dir/$entry";
            if (-d $path) {
                find_res_files_recursive($path, $files_ref);
            } elsif ($entry =~ /\.res$/) {
                push @$files_ref, $path;
            }
        }
    }

    find_res_files_recursive($directory, \@res_files);
    return @res_files;
}

sub get_res_from_id {
    my ($id) = @_;
    my @all_resources = get_values_for_category("Resources");
	my $resource = $all_resources[$id];
	my @infos = split(":", $resource);

    return @infos;
}

sub get_all_res {
    my @lines = get_values_for_category("Resources");
    my $res = {};
    
    for my $line (@lines) {
        chomp($line); # Remove newline characters
        my ($path, $port) = split(":", $line);
        $res->{$path} = $port;
    }
    
    return $res;
}

1;