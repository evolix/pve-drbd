package PVE::DRBD::Config::Parser;

use warnings;
use strict;

sub save_to_file {
    my ($data, $filename) = @_;

    open(my $fh, '>', $filename) or die "Impossible d'ouvrir le fichier '$filename' en écriture : $!";
    _write_hash_to_file($fh, $data, 0);
    close($fh);
}

sub _write_hash_to_file {
    my ($fh, $data, $indent_level) = @_;

    for my $key (sort keys %{$data}) {
        my $value = $data->{$key};
        if (ref($value) eq 'HASH') {
            print $fh ' ' x $indent_level . "$key {\n";
            _write_hash_to_file($fh, $value, $indent_level + 4);
            print $fh ' ' x $indent_level . "}\n";
        } elsif (ref($value) eq 'ARRAY') {
            for my $line (@$value) {
                print $fh ' ' x $indent_level . "$line\n";
            }
        }
    }
}


sub parse_drbdfile {
    my ($path) = @_;

    my $fd;
    open($fd, '<', $path);

    my @lines = <$fd>;
    
    close ($fd);

    my $index = 0;

    return parser(\$index, @lines);
}
sub parser {
    my ($ref_index, @lines) = @_;

    my %data = ();
    while ($$ref_index < scalar @lines) {
        my $line = $lines[$$ref_index];
        $line =~ s/^\s+|\s+$//g;
        die "Invalid {} in the same line: $line" if ($line =~ /\{/ && $line =~ /\}/);
        if ($line =~ /\{/) {
            my $key = $line;
            $key =~ s/\s*\{\s*$//; # Supp l'accolade de fin
            $data{$key} = {};
            $$ref_index += 1;
            my %sub_data = parser($ref_index, @lines);
            $data{$key} = \%sub_data; # Ajt la ref du sous tableau
        } elsif ($line =~ /\}/) {
            return %data;
        } else {
            push @{$data{'data'}}, $line; # Ajt la ligne a data
        }
        $$ref_index++;
    }
    return \%data;
}

1;