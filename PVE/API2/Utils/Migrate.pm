package PVE::API2::Utils::Migrate;
use strict;
use warnings;

use PVE::RESTHandler;
use PVE::RPCEnvironment;
use base qw(PVE::RESTHandler);
use PVE::DRBD::Utils;
use PVE::SSH;
use PVE::DRBD::Utils::Volume;
use PVE::DRBD::Checker;
__PACKAGE__->register_method ({
    name => 'hot_migrate',
    path => '',
    method => 'PUT',
    permissions => {
        check => ['perm', '/', [ 'Sys.Audit', 'Sys.Modify', 'Sys.AccessNetwork' ]],
    },
    protected => 1,
    description => "Perform a hot migration of a VM that uses DRBD. This manipulates the disk's related DRBD resources to add the destination node and set it as primary with the VM (it use qm migrate for data migration).",
    parameters => {
        type => 'object',
        additionalProperties => 0,
        properties => {
            vmid => {
                description => "The VMID of the VM you want to hot migrate. The VM must be running.",
                type => 'integer',
            },
            src_node => {
                description => "Hostname of the current VM owner.",
                type => 'string',
            },
            dst_node => {
               description => "Destination node. This will be the node that owns the VM and will be primary on the resources.",
                type => 'string',
            },
            create => {
                description => "Set to true (or 1) if you want to create the logical volume (LV) associated with the VM resources on the destination node and put DRBD metadata in it. It might not work and could leave the /etc/drbd.d/tmpres.res file. Use it to manually create metadata if it fails.",
                type => 'integer',
                optional => 1,
                default => 0,
            },
        },
    },
    returns => {
        type => 'object',
        properties => {
            status => {
                type => 'integer',
                description => "DRBD file status",
            },
            message => {
                description => "message of status",
                type => 'string',
            },
        },
    },
    code => sub {
        my ($param) = @_;

        my $toadd = $param->{dst_node};
        my $srcnode = $param->{src_node};
        my $vmid = $param->{vmid};

        my $renv = PVE::RPCEnvironment::get();
        my $authuser = $renv->get_user();

        my $realcmd = sub {

            my $pveres = PVE::DRBD::Config::get_all_res(); #get all res with associated port

            my $storages = PVE::DRBD::Utils::get_vm_storage_formated($vmid);

            print("Geting all resources path\n");
            my @res_paths;
            for my $storage (keys %{$storages}){
                my $storage_config = PVE::DRBD::Utils::get_storage_config($storage);
                my $res_name = $storage_config->{res_name};
                next if (!$res_name);
                my $rescfg = PVE::DRBD::Utils::get_cfg_from_name($res_name);
                push @res_paths, $rescfg->{path} if ($rescfg->{path} ne "");
            }

            die "No resources find in attached storages, take a look at storages link to VM, and resource in the res_name property of storages." if (scalar @res_paths == 0);
            print("Checking if destination node can be added to resources\n");
            for my $path (@res_paths){
                my $cfg = PVE::DRBD::Resource::loadConf($path);

                my $nodes_data = PVE::DRBD::Resource::getValues($cfg, "node");
                my @nodes_keys = keys %{$nodes_data};
            
                my $doContain = 0;
                for my $node (@nodes_keys){
                    my ($key, $name) = split(" ", $node);
                    $doContain = 1 if ($name eq $toadd);
                }

                if (!$doContain && scalar(@nodes_keys) == 2) {
                    return {status => 3, message => "Resource $path: File already contains two nodes, can't add $toadd to it."};
                }
            }

            print("Configuring resources and starting them\n");
            for my $path (@res_paths){
                my $port = $pveres->{$path};

                my $fcfg = PVE::DRBD::Resource::loadConf($path);

                PVE::DRBD::Resource::addNode($fcfg, $toadd, $port);
                PVE::DRBD::Resource::addValue($fcfg, "net", "allow-two-primaries");
                my $isValid = PVE::DRBD::Checker::check_resource($fcfg);
                return $isValid if ($isValid->{status} != 1);
                PVE::DRBD::Resource::saveConf($path, $fcfg);

                eval { PVE::DRBD::Utils::Volume::create_lv_node($path, $toadd) if ($param->{create} == 1); };
                return {status => 2, message => "$@"} if ($@);
                
                my $resName = PVE::DRBD::Resource::get_res_name($fcfg);
                PVE::SSH::run_command_nodes("cp $path /etc/drbd.d/", [$srcnode, $toadd]);
                PVE::SSH::run_command_nodes("/usr/sbin/drbdadm adjust $resName ", [$srcnode, $toadd]);
                PVE::SSH::run_command_nodes("/usr/sbin/drbdadm primary $resName --force", [$toadd]);
                sleep(2);
            }

            print("WAITING PEERS TO CONNECT EACHOTHER\n");
            my $retry = 0;
            while (1) {
                my $status = PVE::DRBD::Utils::parse_drbd_status($srcnode);
                my $allDone = 1;
                for my $path (@res_paths) {
                    my $resname = PVE::DRBD::Utils::get_path_res_name($path);
                    $allDone = 0 if ($status->{$resname}->{"peers"}->[0]->{connection} ne "Connected");
                }
                last if ($allDone);
                die "ERROR: Waiting conneciton between nodes expires." if ($retry++ == 5);
                sleep(1);
            }

            print("WAITING SYNC TO FINISH\n");
            while (1) {
                my $status = PVE::DRBD::Utils::parse_drbd_status($srcnode);
                my $isfinish = 1;
                for my $path (@res_paths) {
                    my $resname = PVE::DRBD::Utils::get_path_res_name($path);
                    $isfinish = 0 if ($status->{$resname}->{"peers"}->[0]->{replication} ne "Established");
                }
                last if ($isfinish);
                sleep(1);
            }

            print("Performing qemu migration\n");
            PVE::SSH::run_command_nodes("/usr/sbin/qm migrate $vmid $toadd --online", [$srcnode]);
        };
        $renv->fork_worker('drbdhotmigrate', undef, $authuser, $realcmd);
        return {status => 1, message => ""};
    },
});


__PACKAGE__->register_method ({
    name => 'drbd_danse',
    path => 'danse',
    method => 'PUT',
    permissions => {
        check => ['perm', '/', [ 'Sys.Audit', 'Sys.Modify', 'Sys.AccessNetwork' ]],
    },
    protected => 1,
    description => "Perform the DRBD danse, which is a quick way to switch VM resources nodes and owner.",
    parameters => {
        type => 'object',
        additionalProperties => 0,
        properties => {
            vmid => {
                description => "The VMID of the VM you want to transfer. The VM can be started or down.",
                type => 'integer',
            },
            first_node => {
                description => "Hostname of the future VM owner and primary node of all VM's related resources.",
                type => 'string',
            },
            second_node => {
               description => "Second node to add to the VM's resources, to sync data with.",
                type => 'string',
            },
            create => {
                description => "Set to true (or 1) if you want to create the logical volume (LV) associated with the VM resources on the destination node and put DRBD metadata in it. It might not work and could leave the /etc/drbd.d/tmpres.res file. Use it to manually create metadata if it fails.",
                type => 'integer',
                optional => 1,
                default => 0,
            },
        },
    },
    returns => {
        type => 'object',
        properties => {
            status => {
                type => 'integer',
                description => "DRBD file status",
            },
            message => {
                description => "message of status",
                type => 'string',
            },
        },
    },
    code => sub {
        my ($param) = @_;

        my $renv = PVE::RPCEnvironment::get();
        my $authuser = $renv->get_user();

        my $realcmd = sub {
            my $newnodes = [$param->{first_node}, $param->{second_node}];
            my $vmid = $param->{vmid};
            my $mainNode = "";


            #-----------GET ALL RES PATH--------------
            print("GETTING ALL RES PATH\n");
            my @res_paths;
            my $storages = PVE::DRBD::Utils::get_vm_storage_formated($vmid);
            for my $storage (keys %{$storages}){
                my $storage_config = PVE::DRBD::Utils::get_storage_config($storage);
                my $res_name = $storage_config->{res_name};
                my $rescfg = PVE::DRBD::Utils::get_cfg_from_name($res_name);
                push @res_paths, $rescfg->{path} if ($rescfg->{path} ne "");
            }

            #----------GET PRIMARY ON ALL RES-----------
            my $conf_dir = PVE::DRBD::Config::get_conf_dir();

            print("GET PRIMARY ON ALL RES\n");
            my $format_res = {};
            for my $path (@res_paths) {
                my $resname = PVE::DRBD::Utils::get_path_res_name($path);
                my $primary_nodes = PVE::DRBD::Utils::get_res_primary($path);

                die "Error: No primary node on resource $resname." if (!$primary_nodes);
                die "Error: Multiple primary nodes on resource $resname. Please select only one." if (scalar @$primary_nodes > 1);

                my ($filename) = $path =~ /\/([^\/]+)$/;
                print("filename: $filename|\n");
                $mainNode = $primary_nodes->[0];
                $format_res->{$resname}->{pve_path} = $path;
                $format_res->{$resname}->{drbd_path} = "/etc/drbd.d/$filename";
                $format_res->{$resname}->{tmp_path} = "$conf_dir/tmp/$filename";
                $format_res->{$resname}->{oldprimary} = $primary_nodes->[0];
                $format_res->{$resname}->{newprimary} = $newnodes->[0];
            }

            die "Old primary and New primary node can't be the same node $mainNode." if ($mainNode eq $newnodes->[0]);

            #--------------REMOVING ALL OTHERS NODES FROM FILES---------
            print("REMOVING ALL OTHERS NODES FROM FILES\n");
            for my $resname (keys %{$format_res}) {
                my $cfg = PVE::DRBD::Resource::loadConf($format_res->{$resname}->{pve_path});

                my $node_cat = PVE::DRBD::Resource::getNodes($cfg);
                my $nodes = PVE::DRBD::Utils::get_nodes_name($node_cat);

                for my $node (@$nodes) {
                    if ($node ne $format_res->{$resname}->{oldprimary}) {
                        PVE::DRBD::Resource::delNode($cfg, $node) ;
                        PVE::DRBD::Utils::Resource::stop($format_res->{$resname}->{pve_path}, $resname, [$node]);
                    }
                }
                PVE::DRBD::Resource::saveConf($format_res->{$resname}->{tmp_path}, $cfg);
            }

            #---------------ADDING FIRST NEW NODE TO TRANSFERT DATA------------
            print("ADDING FIRST NEW NODE TO TRANSFERT DATA\n");
            for my $resname (keys %{$format_res}) {
                my $cfg = PVE::DRBD::Resource::loadConf($format_res->{$resname}->{tmp_path});

                my $port = PVE::DRBD::Resource::getPort($cfg);
                PVE::DRBD::Resource::addNode($cfg, $format_res->{$resname}->{newprimary} , $port);
                PVE::DRBD::Resource::saveConf($format_res->{$resname}->{tmp_path}, $cfg);

                my $create_res = PVE::DRBD::Utils::Volume::create_lv_node($format_res->{$resname}->{tmp_path}, $format_res->{$resname}->{newprimary}) if ($param->{create});
                return $create_res if ($param->{create} && $create_res->{status} != 1);
            }
            sleep (1);
            #---------------STARTING RES & TRANSFERING------------
            print("STARTING RES & TRANSFERING\n");
            for my $resname (keys %{$format_res}) {
                PVE::DRBD::Utils::Resource::start($format_res->{$resname}->{tmp_path}, $resname);
            }

            # ##################WAINTING PEERS TO CONNECT EACHOTHER########################
            print("WAITING PEERS TO CONNECT EACHOTHER\n");

            my $retry = 0;
            while (1) {
                my $status = PVE::DRBD::Utils::parse_drbd_status($newnodes->[0]);
                my $allDone = 1;
                for my $resname (keys %{$format_res}) {
                    $allDone = 0 if (exists $status->{$resname} && $status->{$resname}->{"peers"}->[0]->{connection} ne "Connected");
                }
                last if ($allDone);
                die "ERROR: Waiting conneciton between nodes expires." if ($retry++ == 5);
                sleep(1);
            }

            print("WAITING SYNC TO FINISH\n");
            # ##################WAITING FOR SYNCS FINISH########################
            while (1) {
                my $status = PVE::DRBD::Utils::parse_drbd_status($newnodes->[0]);
                my $isfinish = 1;
                for my $resname (keys %{$format_res}) {
                    $isfinish = 0 if (exists $status->{$resname} && $status->{$resname}->{"peers"}->[0]->{replication} ne "Established");
                }
                last if ($isfinish);
                sleep(1);
            }
            # ##########################################################################

            ########################SHUT DOWN VM ON OLDPRIMARY########################################
            print("Shut down VM\n");
            PVE::SSH::run_command_nodes("/usr/sbin/qm stop $vmid", [$mainNode]);

            print("Copy config\n");
            PVE::SSH::run_command_nodes("mv /etc/pve/qemu-server/$vmid.conf /etc/pve/nodes/".$newnodes->[0]."/qemu-server/", [$mainNode]);

            print("Switching roles\n");
            for my $resname (keys %{$format_res}) {
                PVE::DRBD::Utils::Resource::status($resname, $format_res->{$resname}->{oldprimary}, "secondary");
                PVE::DRBD::Utils::Resource::status($resname, $format_res->{$resname}->{newprimary}, "primary");
            }

            ########################START VM ON NEWPRIMARY ########################################
            print("Starting VM\n");
            PVE::SSH::run_command_nodes("/usr/sbin/qm start $vmid", [$newnodes->[0]]);

            print("Desactivate old primay\n");
            for my $resname (keys %{$format_res}) {
                PVE::SSH::run_command_nodes("/usr/sbin/drbdadm down $resname", [$mainNode]);
                PVE::SSH::run_command_nodes("/usr/bin/rm ".$format_res->{$resname}->{drbd_path}, [$mainNode]);
            }

            print("Removing old primary & adding new nodes\n");
            for my $resname (keys %{$format_res}) {
                my $cfg = PVE::DRBD::Resource::loadConf($format_res->{$resname}->{tmp_path});
                my $port = PVE::DRBD::Resource::getPort($cfg);

                PVE::DRBD::Resource::delNode($cfg, $format_res->{$resname}->{oldprimary});
                PVE::DRBD::Utils::Resource::stop($format_res->{$resname}->{tmp_path}, $resname, [$format_res->{$resname}->{oldprimary}]);
                for my $new (@$newnodes) {
                    if ($new ne $format_res->{$resname}->{newprimary}) {
                        PVE::DRBD::Resource::addNode($cfg, $new , $port);
                        PVE::DRBD::Resource::saveConf($format_res->{$resname}->{tmp_path}, $cfg);
                        PVE::DRBD::Utils::Volume::create_lv_node($format_res->{$resname}->{tmp_path}, $new) if ($param->{create});
                        sleep(1);
                    }
                }

            }

            print("Refreshing resource\n");
            for my $resname (keys %{$format_res}) {
                my $cfg = PVE::DRBD::Resource::loadConf($format_res->{$resname}->{tmp_path});

                PVE::DRBD::Utils::Resource::start($format_res->{$resname}->{tmp_path}, $resname);

                PVE::DRBD::Resource::saveConf($format_res->{$resname}->{pve_path}, $cfg);
            }
        };

        $renv->fork_worker('drbddanse', undef, $authuser, $realcmd);
        return {status => 1, message => ""};
        
    },
});


1;