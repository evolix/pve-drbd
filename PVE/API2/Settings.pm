package PVE::API2::Settings;

use strict;
use warnings;

use PVE::DRBD::Utils;
use PVE::DRBD::Config;
use base qw(PVE::RESTHandler);

__PACKAGE__->register_method ({
    name => 'get_settings',
    path => '',
    method => 'GET',
    permissions => {
        check => ['perm', '/', [ 'Sys.Audit' ]],
    },
    protected => 1,
    description => "Get the current settings from the configuration file.",
    parameters => {
        additionalProperties => 0,
        properties => {},
    },
    returns => {
        type => 'array',
        items => {
            type => 'object',
            additionalProperties => 0,
            properties => {
                option => {
                    type => 'string',
                    description => "DRBD file status.",
                },
                value => {
                    type => 'string',
                    description => "DRBD file status.",
                },
            }
        }
    },
    code => sub {
        my $ret = [];

        my $conf = PVE::DRBD::Config::read_conf_file();

        for my $setting (@{$conf->{"Settings"}}){
            my ($key, $value) = split(":", $setting);

            if ($key eq "Version") { #Auto refresh drbd version
                my $version = PVE::DRBD::Utils::get_drbd_version();
                if ($version) {
                    $setting = "Version:".$version;
                    $value = $version;
                } 
            }

            my $res = {
                option => $key,
                value => $value,
            };
            push @$ret, $res;
        }
        PVE::DRBD::Config::write_conf_file($conf);

        return $ret;
    }
});


__PACKAGE__->register_method ({
    name => 'set_setting',
    path => '',
    method => 'POST',
    permissions => {
        check => ['perm', '/', [ 'Sys.Audit', 'Sys.Modify']],
    },
    protected => 1,
    description => "Update a specific setting in the configuration file.",
    parameters => {
        type => 'object',
        additionalProperties => 0,
        properties => {
            option => {
                description => "Setting option.",
                type => 'string',
            },
            newvalue => {
                description => "New setting value.",
                type => 'string',
            },
        }
    },
    returns => {
        type => 'object',
        properties => {
            status => {
                description => "Status of the operation.",
                type => 'integer',
            },
            message => {
                description => "Message indicating the status.",
                type => 'string',
            },
        },
    },
    code => sub {
        my ($param) = @_;


        my $conf = PVE::DRBD::Config::read_conf_file();

        return {status => 2, message => "No conf file."} if (! ($conf));
        return {status => 2, message => "No settings in conf file."} if (!( exists $conf->{"Settings"}));

        my $option = $param->{option};
        my $newvalue = $param->{newvalue};

        my $notValid = PVE::DRBD::Checker::check_opt({$option => $newvalue});
        return $notValid if ($notValid->{status} != 1);

        my $illegal = {
            Version => " link to actual the DRBD kernel version",
        };
        return {status => 3, message => "Can't change $option: ".$illegal->{$option}} if (exists $illegal->{$option});


        my $doExist = 0;
        for my $setting (@{$conf->{"Settings"}}){
            my ($setopt, $setvalue) = split(":", $setting, 2);
            if ($setopt eq $option) {
                $setting = $option.":".$newvalue; 
                $doExist = 1;
            }
        }

        return {status => 2, message => "Option '$option' don't exist in conf file (/etc/pve/pve-drbd/conf.cfg)"} if (! ($doExist));
        PVE::DRBD::Config::write_conf_file($conf);

        return {status => 1, message => ""};
    }
});

1;