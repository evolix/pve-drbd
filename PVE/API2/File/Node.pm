package PVE::API2::File::Node;

use strict;
use warnings;

use PVE::RESTHandler;
use base qw(PVE::RESTHandler);
use PVE::DRBD::Resource;
use PVE::DRBD::Utils::Volume;
use PVE::DRBD::Checker;

__PACKAGE__->register_method ({
    name => 'get_nodes',
    path => '{res_name}',
    method => 'GET',
    permissions => {
        check => ['perm', '/', [ 'Sys.Audit' ]],
    },    
    description => "Retrieve the index of DRBD nodes.",
    parameters => {
        additionalProperties => 0,
        properties => {
            res_name => {
                description => "Resource name, defined at the top of the resource file (resource xxx {).",
                type => 'string',
            },
        },
    },
    returns => {
        type => 'array',
        items => {
            type => "object",
            properties => {
                node => {
                    description => "Node ID.",
                    type => 'string',
                },
                address => {
                    description => "Address of the node.",
                    type => 'string',
                },
            },
        },
    },
    code => sub {
        my ($param) = @_;

        my $res_name = $param->{res_name};
        my $cfg = PVE::DRBD::Utils::get_cfg_from_name($res_name);

        return {status => 4, message => "Can't find ".$cfg->{path}.", verify that file exist, change path in conf.cfg file or remove $res_name index."} if (!defined $cfg->{path});

		my $data = PVE::DRBD::Resource::getValues($cfg->{cfg}, "node");
        my $ret = [];
        for my $keys (keys %{$data})
        {
            my $hostname = (split(" ", $keys))[1];
            my $address = "";

            for my $line (@{$data->{$keys}}){
                if ($line =~ /address /){
                    $address = (split(" ", $line, 2))[1];
                }
            }
            my $tab = {node => $hostname, address => $address};
            push @$ret, $tab;
        }
        return $ret;
    }
});

__PACKAGE__->register_method ({
    name => 'remove_node',
    path => '{res_name}',
    method => 'DELETE',
    permissions => {
        check => ['perm', '/', [ 'Sys.Audit', 'Sys.Modify' ]],
    },
    protected => 1,
    description => "Remove a DRBD node.",
    parameters => {
        type => 'object',
        additionalProperties => 0,
        properties => {
            node => {
                description => "Hostname of the node to remove.",
                type => 'string',
            },
            delete => {
                description => "Set to true (or 1) if you want to delete the logical volume (LV) associated with the current resource on the node.",
                type => 'integer',
                optional => 1,
                default => 0,
            },
            res_name => {
                description => "Resource name, defined at the top of the resource file (resource xxx {).",
                type => 'string',
            },
        },
    },
    returns => {
        type => "object",
        properties => {
            status => {
                description => "Status of the operation.",
                type => 'integer',
            },
            message => {
                description => "Message indicating the status.",
                type => 'string',
            },
        },
    },
    code => sub {
        my ($param) = @_;
        my $hostname = $param->{node};
        my $doCreate = $param->{delete};

        my $res_name = $param->{res_name};
        my $cfg = PVE::DRBD::Utils::get_cfg_from_name($res_name);

        return {status => 4, message => "Can't find ".$cfg->{path}.", verify that file exist, change path in conf.cfg file or remove $res_name index."} if (!defined $cfg->{path});
        PVE::DRBD::Utils::Volume::delete_lv_node($cfg->{path}, $hostname) if ($doCreate && $doCreate == 1);

        PVE::DRBD::Resource::delValue($cfg->{cfg}, "node", $hostname);
        PVE::DRBD::Resource::saveConf($cfg->{path}, $cfg->{cfg});
        return { status => 1, message => "volume $hostname has been successfully removed." };
    }
});

__PACKAGE__->register_method ({
    name => 'create_lv_node',
    path => '{res_name}',
    method => 'PUT',
    permissions => {
        check => ['perm', '/', [ 'Sys.Audit', 'Sys.Modify']],
    },
    protected => 1,
    description => "Create a DRBD node.",
    parameters => {
        type => 'object',
        additionalProperties => 0,
        properties => {
            node => {
                description => "Hostname of the node to create.",
                type => 'string',
            },
            create => {
                description => "Set to true (or 1) if you want to create the logical volume (LV) associated with the current resource on the node and put DRBD metadata in it. It might not work and could leave the /etc/drbd.d/tmpres.res file. Use it to manually create metadata if it fails.",
                type => 'integer',
                optional => 1,
                default => 0,
            },
            res_name => {
                description => "Resource name, defined at the top of the resource file (resource xxx {).",
                type => 'string',
            },
        },
    },
    returns => {
        type => "object",
        properties => {
            status => {
                description => "Status of the operation.",
                type => 'integer',
            },
            message => {
                description => "Message indicating the status.",
                type => 'string',
            },
        },
    },
    code => sub {
        my ($param) = @_;

        my $hostname = $param->{node};
        my $res_name = $param->{res_name};
        my $cfg = PVE::DRBD::Utils::get_cfg_from_name($res_name);
        
        my $doCreate = $param->{create};

         return {status => 4, message => "Can't find ".$cfg->{path}.", verify that file exist, change path in conf.cfg file or remove $res_name index."} if (!defined $cfg->{path});

        my $status = PVE::DRBD::Resource::addValue($cfg->{cfg}, "node", $hostname, $cfg->{port});
        return {status => 3, message => "Node $hostname is invalid, be sure that it exist in /etc/hosts file."} if ($status != 1);

        my $isValid = PVE::DRBD::Checker::check_resource($cfg->{cfg});
        return $isValid if ($isValid->{status} != 1);

        PVE::DRBD::Resource::saveConf($cfg->{path}, $cfg->{cfg});

        eval { PVE::DRBD::Utils::Volume::create_lv_node($cfg->{path}, $hostname)  if ($doCreate && $doCreate == 1); };
        return { status => 2, message => "$@"} if ($@);
        return { status => 1, message => "Node $hostname has been successfully added." };
    

    }
});

1;