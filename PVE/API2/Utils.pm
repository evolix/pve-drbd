package PVE::API2::Utils;

use strict;
use warnings;

use PVE::DRBD::Utils;
use base qw(PVE::RESTHandler);
use PVE::SSH;
use PVE::DRBD::Config;
use PVE::Tools qw(trim);

__PACKAGE__->register_method ({
    name => 'get_vm_list',
    path => 'vms',
    method => 'GET',
    permissions => {
        check => ['perm', '/', [ 'Sys.Audit' ]],
    },
    protected => 1,
    description => "Get all DRBD VMs on all nodes. Returns the VMID and the owner's node.",
    parameters => {
        type => 'object',
        additionalProperties => 0,
    },
    returns => {
        type => 'array',
        items => {
            type => "object",
            properties => {
                vmid => {
                    description => "The VMID of VM.",
                    type => 'string',
                },
                node => {
                    description => "The owner node of the VM, and the primary node on all VM's resources",
                    type => 'string',
                },
            },
        },
    },
    code => sub {
        my $ret = [];
        my $vm_list = PVE::DRBD::Utils::get_vm_list();

        for my $vm (keys %{$vm_list}){
            my $storages = PVE::DRBD::Utils::get_vm_storage_formated($vm);

            for my $storage (keys %{$storages}){
                my $sto_cfg = PVE::DRBD::Utils::get_storage_config($storage);
                next if ($sto_cfg->{type} ne "drbd");
                push @$ret, {vmid => $vm, node => $vm_list->{$vm}->{node}};
                last;
            }
        }
        return $ret;
    },
});


__PACKAGE__->register_method ({
    name => 'is_drbddir_clean',
    path => 'isclean',
    method => 'GET',
    permissions => {
	check => ['perm', '/', [ 'Sys.Audit' ]],
    },
    protected => 1,
    description => "Check on all cluster nodes if a resource file is in the DRBD directory (/etc/drbd.d) and not linked with pvedrbd (don't exists in the pve-drbd directory and isn't defined in the configuration file). If not, please consider deleting it or manually linking it.",
    parameters => {
        type => 'object',
        additionalProperties => 0,
    },
    returns => {
        type => "object",
        properties => {
            status => {
                description => "Status of the operation.",
                type => 'integer',
            },
            message => {
                description => "Message indicating the status.",
                type => 'string',
            },
        },
    },
    code => sub {

        my $drbddir = '/etc/drbd.d';

        my $drbdfiles = {};
        my @pvefilespath = PVE::DRBD::Config::get_values_for_category("Resources");

        my @nodes = PVE::DRBD::Utils::get_all_nodes();
        PVE::SSH::run_command_nodes("ls $drbddir | awk '{print}'", \@nodes, outfunc => sub {
            my ($node, $line) = @_;

            $line = trim($line);
            push @{$drbdfiles->{$node}}, $line if ($line =~ /\.res$/);
        });


        my $dirty_res = "";
        for my $node (keys %{$drbdfiles}) {
            for my $drbdfile (@{$drbdfiles->{$node}}) {
                my $doFind = 0;
                for my $pvefile (@pvefilespath) {
                    my ($path, $port) = split(":", $pvefile);
                    my ($pvename) = $path =~ /\/([^\/]+)$/;

                    $doFind = 1 if ($drbdfile eq $pvename);
                }
                $dirty_res .= ", " if ((! $doFind) && $dirty_res ne "");
                $dirty_res .= "$drbdfile" if (! $doFind);
            }
            return {status => 2, message => "Node $node: Find unlink resource(s) in '$drbddir': $dirty_res. If you don't want to use them, please delete or maunually add them to ".PVE::DRBD::Config::get_setting_value("Resources directory")." dir and conf.cfg file to avoid conflict. If you are performing a drbd action (migration / danse), ignore this until it finish."} if ($dirty_res ne "");
        }


        return {status => 1, message => ""};
    },
});



1;
