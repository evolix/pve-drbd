package PVE::API2::File;

use strict;
use warnings;

use PVE::RESTHandler;
use base qw(PVE::RESTHandler);
use PVE::DRBD::Resource;
use PVE::DRBD::Config;
use PVE::DRBD::Checker;

__PACKAGE__->register_method ({
    name => 'del_category',
    path => 'category/{res_name}',
    method => 'DELETE',
    permissions => {
        check => ['perm', '/', [ 'Sys.Audit', 'Sys.Modify']],
    },
    protected => 1,
    description => "Delete a value from the specified category.",
    parameters => {
        type => 'object',
        additionalProperties => 0,
        properties => {
            res_name => {
                description => "Resource ID",
                type => 'string',
            },
            category => {
                description => "Resource category",
                type => 'string',
            },
            value => {
                description => "Value to be removed from the category.",
                type => 'string',
            },
        },
    },
    returns => {
        type => "object",
        properties => {
            status => {
                description => "Status of the operation.",
                type => 'integer',
            },
            message => {
                description => "Message indicating the status.",
                type => 'string',
            },
        },
    },
    code => sub {
        my ($param) = @_;

        my $res_name = $param->{res_name};
        my $cfg = PVE::DRBD::Utils::get_cfg_from_name($res_name);

         return {status => 4, message => "Can't find ".$cfg->{path}.", verify that file exist, change path in conf.cfg file or remove $res_name index."} if (!defined $cfg->{path});

        PVE::DRBD::Resource::delValue($cfg->{cfg}, $param->{category}, $param->{value});
        PVE::DRBD::Resource::saveConf($cfg->{path}, $cfg->{cfg});
        return {status => 1, message => ""};
    }
});

__PACKAGE__->register_method ({
    name => 'new_category',
    path => 'category/{res_name}',
    method => 'POST',
    permissions => {
        check => ['perm', '/', [ 'Sys.Audit', 'Sys.Modify' ]],
    },
    protected => 1,
    description => "Insert a new value into a specific category of the resource. The ';' ending is added automatically after the request, so you don't need to include it.",
    parameters => {
        type => 'object',
        additionalProperties => 0,
        properties => {
            res_name => {
                description => "Resource name, defined at the top of the resource file (resource xxx {).",
                type => 'string',
            },
            category => {
                description => "A specific category of the resource.",
                type => 'string',
            },
            value => {
                description => "Value you want to add.",
                type => 'string',
            },
        },
    },
    returns => {
        type => "object",
        properties => {
            status => {
                description => "Status of the operation.",
                type => 'integer',
            },
            message => {
                description => "Message indicating the status.",
                type => 'string',
            },
        },
    },
    code => sub {
        my ($param) = @_;

        my $res_name = $param->{res_name};
        my $cfg = PVE::DRBD::Utils::get_cfg_from_name($res_name);

        return {status => 4, message => "Can't find ".$cfg->{path}.", verify that file exist, change path in conf.cfg file or remove $res_name index."} if (!defined $cfg->{path});

        my $status = PVE::DRBD::Resource::addValue($cfg->{cfg}, $param->{category}, $param->{value});
        return {status => 3, message => "ERROR: Can't add ".$param->{value}." in ".$param->{category}."."} if ($status != 1);

        my $isValid = PVE::DRBD::Checker::check_resource($cfg->{cfg});
        return $isValid if ($isValid->{status} != 1);
    
        PVE::DRBD::Resource::saveConf($cfg->{path}, $cfg->{cfg});
        return {status => 1, message => ""};
    }
});

__PACKAGE__->register_method ({
    name => 'get_category',
    path => 'category/{res_name}',
    method => 'GET',
    permissions => {
        check => ['perm', '/', [ 'Sys.Audit' ]],
    },
    protected => 1,
    description => "Retrieve all values in the specified category of the resource 'res_name'. The values are formatted as an array ({value => 'value'}, {value => 'value2'}, ...).",
    parameters => {
        type => 'object',
        additionalProperties => 0,
        properties => {
            res_name => {
                description => "Resource name, defined at the top of the resource file (resource xxx {).",
                type => 'string',
            },
            category => {
                description => "A specific category of the resource.",
                type => 'string',
            },
        },
    },
    returns => {
        type => 'array',
        items => {
            type => "object",
            additionalProperties => 1, # Permet des propriétés dynamiques
        },
    },
    code => sub {
        my ($param) = @_;

        my $ret = [];
        my $res_name = $param->{res_name};
        my $cfg = PVE::DRBD::Utils::get_cfg_from_name($res_name);

        return {status => 4, message => "Can't find ".$cfg->{path}.", verify that file exist, change path in conf.cfg file or remove $res_name index."} if (!defined $cfg->{path});
        my $category = $param->{category}; 

        my $data = PVE::DRBD::Resource::getValues($cfg->{cfg}, $category);

        my %entry = ();
        for my $cat (keys %{$data}) {
            if (ref $data->{$cat} eq 'ARRAY') {
                my ($key, $val) = split(" ", $cat);
                my @values = @{$data->{$cat}};
                my %entr = ();

                $entr{$key} = $val;
                for my $value (@values) {
                    my ($key, $val) = split(" ", $value);
                    next if (!defined($key) || $key eq "");
                    $entr{$key} = $val;
                }
                push @$ret, \%entr if %entr;
            } else {
                # Handle case where data is in the form "key value;"
                push @$ret, {value => $cat} if $cat;
            }
        }
        $entry{value} = "" if (scalar @$ret == 0);
        push @$ret, \%entry if %entry;

        return $ret;
    }
});

__PACKAGE__->register_method ({
    name => 'get_categories',
    path => 'categories',
    method => 'GET',
    permissions => {
	    check => ['perm', '/', [ 'Sys.Audit' ]],
    },
    protected => 1,
    description => "Retrieve all existing categories in the specified resource. Note: The 'node' category includes all nodes (on xxx).",
    parameters => {
        type => 'object',
        additionalProperties => 0,
        properties => {
            res_name => {
                description => "Resource name, defined at the top of the resource file (resource xxx {).",
                type => 'string',
            },
        },
    },
    returns => {
        type => 'array',
        items => {
            type => "object",
            properties => {
                category => {
                    description => "Category name.",
                    type => 'string',
                },
            },
        },
    },
    code => sub {
        my ($param) = @_;

        my $ret = [];
        my $res_name = $param->{res_name};
        my $cfg = PVE::DRBD::Utils::get_cfg_from_name($res_name);

        my $formatname = PVE::DRBD::Resource::format_res($res_name);
         return {status => 4, message => "Can't find ".$cfg->{path}.", verify that file exist, change path in conf.cfg file or remove $res_name index."} if (!defined $cfg->{path});

        my @all = ();
        push @$ret, {category => "node"}; #Force node category to exist
        push @all, "node";

        for my $cat (keys %{$cfg->{cfg}->{$formatname}}) {
            $cat = "node" if ((split(" ", $cat))[0] eq "on"); #useless when node is forced
            my $doExist = 0;
            for my $value (@all) {
                $doExist = 1 if ($value eq $cat);
            }
            next if ($doExist eq 1);
            push @all, $cat;
            push @$ret, {category => $cat} if (! (grep { $_{category} && $_{category} eq $cat } @$ret));
        }
        return $ret;
    }
});


__PACKAGE__->register_method ({
    name => 'file',
    path => '',
    method => 'GET',
    permissions => {
        check => ['perm', '/', [ 'Sys.Audit' ]],
    },
    protected => 1,
    description => "Retrieve all DRBD resources set in the conf.cfg file, and provide some information about them.",
    parameters => {
        additionalProperties => 0,
        properties => {},
    },
    returns => {
        type => 'array',
        items => {
            type => 'object',
            additionalProperties => 0,
            properties => {
                status => {
                    type => 'string',
                    description => "DRBD file status.",
                },
                device => {
                    type => 'string',
                    description => "DRBD device status.",
                },
                disk => {
                    type => 'string',
                    description => "DRBD disk path.",
                },
                path => {
                    type => 'string',
                    description => "DRBD file path.",
                },
                name => {
                    type => 'string',
                    description => "Resource name (used for UI selection).",
                },
                port => {
                    type => 'string',
                    description => "Resource port",
                },
                id => {
                    type => 'integer',
                    description => "ID number in the config file (starting from 0).",
                }
            },
        },
    },
    code => sub {

        my $ret = [];
        PVE::DRBD::Config::createConf() if (! PVE::DRBD::Config::conf_exist());
        my @all_res = PVE::DRBD::Config::get_values_for_category("Resources");
        my $index = 0;
        for my $line (@all_res){
            my ($file, $port) = split(":", $line);
            my $res = {};
            $res->{status} = "Fichier inexistant.";
            $res->{path} = $file;
            $res->{id} = $index;
            if (-e $file){
                $res->{status} = "Fichier accessible.";
                my $cfg = PVE::DRBD::Resource::loadConf($file);
                my $res_name= PVE::DRBD::Resource::get_res_name($cfg);
                my $device = PVE::DRBD::Resource::getBase($cfg, "device");
                my $disk = PVE::DRBD::Resource::getBase($cfg, "disk");
                $res->{name} = $res_name;
                $res->{device} = $device;
                $res->{disk} = $disk;
                $res->{port} = $port;
            }
            push @$ret, $res;
            $index++;
        }

        return $ret;        
    }
});

__PACKAGE__->register_method ({
    name => 'file_create',
    path => 'create',
    method => 'PUT',
    permissions => {
        check => ['perm', '/', [ 'Sys.Audit', 'Sys.Modify']],
    },
    protected => 1,
    description => "Create a new DRBD resource with all basic options, ready to be used.",
    parameters => {
        type => 'object',
        additionalProperties => 0,
        properties => {
            name => {
                description => "New resource file name. It's recommended to use the same name as the resource for ease of use.",
                type => 'string',
            },
            res_name => {
                description => "The resource name. This is the value you will use to select and manipulate the resource.",
                type => 'string',
            },
            disk => {
                description => "Resource disk where your future VM disks will be allocated. All nodes in the same DRBD resource must have this disk path accessible. It will wipe the disk and put DRBD metadata on it, so be sure to back up your sensitive data!",
                type => 'string',
            },
            device => {
                description => "The device used for DRBD to manipulate data and sync them. Specify it as '/dev/drbdX' (X is a number).",
                type => 'string',
            },
            port => {
                description => "The port used by the resource to sync data between nodes. The port must be accessible by all future nodes.",
                type => 'integer',
            },
            node => {
                description => "First node in the resource where the disk is located.",
                type => 'string',
            },
        },
    },
    returns => {
        type => 'object',
        properties => {
            status => {
                description => "Status of the operation.",
                type => 'integer',
            },
            message => {
                description => "Message indicating the status.",
                type => 'string',
            },
        },
    },
    code => sub {
        my ($param) = @_;

        my $node = $param->{node};
        my $name = $param->{name};
        my $port = $param->{port};
        my $res_name = $param->{res_name};
        my $device = $param->{device};
        my $disk = $param->{disk};

        my $check_err = PVE::DRBD::Checker::check_opt($param);
        return $check_err if ($check_err->{status} != 1);

        my $res_dir = PVE::DRBD::Config::get_setting_value("Resources directory");
        my $path = "$res_dir/$name.res";

        PVE::DRBD::Resource::createfile($path, $res_name);
        my $cfg = PVE::DRBD::Resource::loadConf($path);

        PVE::DRBD::Resource::addBase($cfg, "device $device");
        PVE::DRBD::Resource::addBase($cfg, "disk $disk");
        PVE::DRBD::Resource::addBase($cfg, "meta-disk internal");
        PVE::DRBD::Resource::addValue($cfg, "node", $node, $port);

        PVE::DRBD::Resource::saveConf($path, $cfg);
        PVE::DRBD::Config::add_value_to_category("Resources", "$path:$port");

        eval { PVE::DRBD::Utils::Volume::create_lv_node($path, $node); };
        return {status => 2, message => "$@"} if ($@);

        return {status => 1, message => "Le fichier ressource a été crée avec succés."};
    }
});

__PACKAGE__->register_method ({
    name => 'file_delete',
    path => 'delete',
    method => 'PUT',
    permissions => {
        check => ['perm', '/', [ 'Sys.Modify', 'Sys.Audit' ]],
    },
    protected => 1,
    description => "Delete a DRBD resource and remove it from the /etc/pve/pve-drbd/conf.cfg file.",
    parameters => {
        type => 'object',
        additionalProperties => 0,
        properties => {
            path => {
                description => "The path of the resource file you want to delete.",
                type => 'string',
            },
        },
    },
    returns => {
        type => 'object',
        properties => {
            status => {
                description => "Status of the operation.",
                type => 'integer',
            },
            message => {
                description => "Message indicating the status.",
                type => 'string',
            },
        },
    },
    code => sub {
        my ($param) = @_;
        my $path = $param->{path};

        my $cfg = PVE::DRBD::Resource::loadConf($path);

        my $resname = PVE::DRBD::Resource::get_res_name($cfg);
        my $nodes = PVE::DRBD::Resource::getValues($cfg, "node");
        my $hosts = PVE::DRBD::Utils::get_nodes_name($nodes);

        my $runMsg = "";
        PVE::SSH::run_command_nodes("drbdadm status $resname", $hosts, outfunc => sub { $runMsg = "$resname is running on node ".$_[0];}, noerr => 1);
        return {status => 2, message => "Can't delete file, $runMsg"} if ($runMsg);

        PVE::DRBD::Config::del_value_from_category("Resources" , $path);
        if (-e $path){
            unlink($path) or return {status => 2, message => "Can't remove file."};
            return {status => 1, message => "Has been removed."};
        }
        return {status => 3, message => "File don't exist."}
    }
});

1;