package PVE::API2::Resource;

use PVE::DRBD::Utils;
use base qw(PVE::RESTHandler);
use PVE::DRBD::Utils::Resource;

__PACKAGE__->register_method ({
    name => 'start',
    path => 'start',
    method => 'GET',
    permissions => {
        check => ['perm', '/', [ 'Sys.Audit', 'Sys.Modify', 'Sys.AccessNetwork' ]],
    },
    protected => 1,
    description => "Start a DRBD resource.",
    parameters => {
        type => 'object',
        additionalProperties => 0,
        properties => {
            res_name => {
               description => "Ressource ID",
                type => 'string',
            },
            restrict => {
                description => "Set to true if you want to start it on current node only. Default to false",
                type => 'integer',
                optional => 1,
                default => 0,
            },
            node => {
                description => "Hostname of the node on which to perform the action. Required if 'restrict' is set to true.",
                type => 'string',
                optional => 1,
                default => "",
            },
        },
    },
    returns => {
	    type => 'object',
        properties => {
            status => {
                type => 'integer',
                description => "start cmd status",
            },
            message => {
                type => 'string',
                description => "msg",
            },
        },
    },
    code => sub {
        my ($param) = @_;
        my $res_name = $param->{res_name};

        my $renv = PVE::RPCEnvironment::get();
        my $authuser = $renv->get_user();

        my $cfg = PVE::DRBD::Utils::get_cfg_from_name($res_name);
        return {status => 2, message => "Can't find file."} if (!defined $cfg->{path});

        my $nodes = $param->{restrict} ? [$param->{node}] : [];

        my $func = sub { 
            my $res = PVE::DRBD::Utils::Resource::start($cfg->{path}, $res_name, $nodes);
            die $res->{message} if ($res->{status} != 1);
        };

        $renv->fork_worker('drbdstart', undef, $authuser, $func);
        return {status => 1, message => ""};
    }
});

__PACKAGE__->register_method ({
    name => 'stop',
    path => 'stop',
    method => 'GET',
    permissions => {
        check => ['perm', '/', [ 'Sys.Audit', 'Sys.Modify', 'Sys.AccessNetwork' ]],
    },
    protected => 1,
    description => "Stop a DRBD resource.",
    parameters => {
        type => 'object',
        additionalProperties => 0,
        properties => {
            res_name => {
               description => "Ressource ID",
                type => 'string',
            },
            restrict => {
                description => "Set to true if you want to start it on current node only. Default to false",
                type => 'integer',
                optional => 1,
                default => 0,
            },
            node => {
                description => "Hostname of the node on which to perform the action. Required if 'restrict' is set to true.",
                type => 'string',
                optional => 1,
                default => "",
            },
        },
    },
    returns => {
	    type => 'object',
    	additionalProperties => 0,
        properties => {
            status => {
                type => 'integer',
                description => "stop cmd status",
            },
            message => {
                type => 'string',
                description => 'return message',
            }
        },
    },
    code => sub {

        my ($param) = @_;

        my $res_name = $param->{res_name};
        my $cfg = PVE::DRBD::Utils::get_cfg_from_name($res_name);

        my $renv = PVE::RPCEnvironment::get();
        my $authuser = $renv->get_user();

        return {status => 2, message => "Can't find file."} if (!defined $cfg->{path});

        my $nodes = $param->{restrict} ? [$param->{node}] : [];

        my $func = sub {
            my $res = PVE::DRBD::Utils::Resource::stop($cfg->{path}, $res_name, $nodes);
            die $res->{message} if ($res->{status} != 1)
        };

        $renv->fork_worker('drbdstop', undef, $authuser, $func);
        return {status => 1, message => ""};
    }
});

__PACKAGE__->register_method ({
    name => 'status_change',
    path => 'status_change',
    method => 'GET',
    permissions => {
        check => ['perm', '/', [ 'Sys.Audit', 'Sys.Modify', 'Sys.AccessNetwork' ]],
    },
    protected => 1,
    description => "Change the status of a node on a specified DRBD resource.",
    parameters => {
        type => 'object',
        additionalProperties => 0,
        properties => {
            res_name => {
               description => "Ressource ID",
                type => 'string',
            },
            node => {
               description => "Node to change",
                type => 'string',
            },
            status => {
               description => "new status",
                type => 'string',
            },
        },
    },
    returns => {
	    type => 'object',
    	additionalProperties => 0,
        properties => {
            status => {
                type => 'string',
                description => "stop cmd status",
            },
            message => {
                type => 'string',
                description => 'return message',
            }
        },
    },
    code => sub {
        my ($param) = @_;

        my $renv = PVE::RPCEnvironment::get();
        my $authuser = $renv->get_user();

        my $res_name = $param->{res_name};
        my $node = $param->{node};
        my $status = $param->{status};

        my $func = sub { 
            my $res = PVE::DRBD::Utils::Resource::status($res_name, $node, $status); 
            die $res->{message} if ($res->{status} != 1)
        };

        $renv->fork_worker('drbdstatus', undef, $authuser, $func);
        return {status => 1, message => ""};
    }
});


__PACKAGE__->register_method ({
    name => 'status',
    path => '',
    method => 'GET',
    permissions => {
        check => ['perm', '/', [ 'Sys.Audit' ]],
    },
    protected => 1,
    description => "Retrieve the status actives DRBD resources on a specified node.",
    parameters => {
        additionalProperties => 0,
        properties => {
            node => {
                description => "Source node",
                type => "string",
            }
        },
    },
    returns => {
        type => 'array',
        items => {
            type => 'object',
            additionalProperties => 0,
            properties => {
                status => {
                    type => 'integer',
                    description => "status",
                },
                connection => {
                    type => 'string',
                    description => "DRBD status message",
                },
                roles => {
                    type => 'string',
                    description => "DRBD nodes status",
                },
                storages => {
                    type => 'string',
                    description => "DRBD storage status",
                },
                res_name => {
                    type => 'string',
                    description => "DRBD resource name",
                }
            },
        }
    },
    code => sub {
        my ($param) = @_;

        my $node = $param->{node};
        return {status => 2, res_name => "Loading..."} if ($node eq "undef");

        my $ret = [];

        my $resources;
        $resources = PVE::DRBD::Utils::parse_drbd_status($node);

        for my $resource (keys %{$resources}) {
            my $res = {};
            $res->{res_name} = $resource;
            
            for my $peer (@{$resources->{$resource}{'peers'}}) {
                $res->{connection} .= $peer->{'replication'}." ";
                if ($peer->{'replication'} eq "SyncSource") {
                    $res->{connection} .= $peer->{'percent'}." %";
                }
                $res->{roles} .= " | " . $peer->{'role'} . " ";
                $res->{storages} .= " | " . $peer->{'peer_disk'} . " ";
            }
            
            $res->{roles} = $resources->{$resource}{'role'} . " " . $res->{roles};
            $res->{storages} = $resources->{$resource}{'disk'} . " " . $res->{storages};
            $res->{status} = 1;
            push @$ret, $res;
        }

        push @$ret, {status => 2, res_name => "No ressource launch !"} if ((scalar (keys %{$resources})) == 0);

        return $ret;
    }
});
