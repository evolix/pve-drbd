# Documentation des Modifications Proxmox pour l'Intégration de DRBD

## 1. Introduction

### Objectif

Cette documentation vise à détailler les modifications apportées au code source de Proxmox pour l'intégration de DRBD. Elle fournit des informations sur les modifications des paquets existants ainsi que sur le nouveau paquet `pve-drbd` créé.

### Contexte

Le module DRBD (Distributed Replicated Block Device) a été intégré à Proxmox pour améliorer la gestion des ressources et des disque pour les VM. Cette intégration permet de manipuler des fichiers et ressources DRBD à travers l'UI de Proxmox VE, et facilité la mis ene place de VM sur des device DRBD.

### Compatibilité

Ce patch est compatible avec drbd8 et drbd9. Il faut donc que le module kernet soit disponible, ainsi que le paquet drbd-utils (>= 9.22).
Vous pouvez trouver un guide d'installation pour drbd9 sur le [Wiki evolix](https://wiki.evolix.org/MultiSlave-DRBD9#installation-%C3%A0-partir-du-repo-de-linstor)

## 2. Vue d'ensemble des modifications

### Résumé

Les modifications apportées comprennent la mise à jour des paquets existants `pve-manager`, `pve-storage`, et `pve-docs`, ainsi que la création d'un nouveau paquet `pve-drbd`.

Vous pouvez récupérer la liste des paquets dans le git officiel de Proxmox : [Proxmox Git](https://git.proxmox.com/)

### Paquets modifiés

- **pve-manager** : Gestion des fonctions et interfaces utilisateur de Proxmox.
  - Interface utilisateur complète avec EXTJS (JavaScript)
  - Base des appels API (Perl), redirigeant ensuite vers les paquets cibles.
- **pve-storage** : Gestion des fonctionnalités de stockage.
  - Définition et fonctionnement des 'storages' (pour la création de disques des VM)
- **pve-docs** : Mise à jour de la documentation pour refléter les nouvelles fonctionnalités.
  - Documentation disponible avec l'API, et auto-générée pour le CLI (`man pve-drbd`)
- **pve-drbd** : Nouveau paquet pour intégrer DRBD à Proxmox.
  - Gestion des appels API relatifs à DRBD
  - CLI `pve-drbd`

## 3. Fonctionnalitées

### Intégration du Stockage de Type DRBD pour les VM Proxmox

Nous avons ajouté le support pour le stockage de type DRBD, permettant d'intégrer de manière transparente l'utilisation de DRBD pour les machines virtuelles sur Proxmox. DRBD est une solution de réplication de bloc au niveau du noyau qui assure la haute disponibilité et la résilience des données en les répliquant en temps réel entre les nœuds du cluster.

### Gestion Simplifiée des Ressources DRBD via l'UI de Proxmox

La gestion des ressources DRBD est entièrement intégrée à l'interface utilisateur de Proxmox, facilitant la création, la modification et la gestion des ressources DRBD. Cette intégration permet aux administrateurs de lier des disques DRBD à des VM directement depuis l'UI, offrant ainsi une expérience utilisateur cohérente et simplifiée. Grâce à cette fonctionnalité, les utilisateurs peuvent rapidement configurer et gérer leurs environnements de stockage répliqué intuitivement.

### Interface en Ligne de Commande (CLI) pve-drbd

En complément de l'interface graphique, un CLI dédiée est également implémenté, pour la gestion avancée des ressources DRBD en les noeuds. La CLI pve-drbd permet aux administrateurs d'exécuter des commandes pour créer, supprimer, modifier et surveiller les ressources DRBD à travers tous les noeuds du cluster, offrant ainsi une flexibilité maximale pour les utilisateurs avancés et les scripts d'automatisation. Cette CLI est particulièrement utile pour les tâches répétitives et les déploiements à grande échelle, où une gestion rapide et efficace est essentielle.

### Fonctionnalités custom

- Une migration à chaud des VM sur la base de DRBD est aussi intégré. Il permet d'ajouter automatiquement le noeud cible aux ressources liés à une machine virtual et d'y effectuer le transfert avec qm.
- Pour drbd8, la danse de drbd est implantée. Elle permet de transferer une VM vers une autre pairs de noeuds, remplaçant les anciens hosts par les nouveaux choisis dans les ressources.

## 4. Instructions d'installation

### Construction à partir des sources

1. Télécharger les sources des paquets ([pve-manager](https://gitea.evolix.org/evolix/pve-manager.git), [pve-storage](https://gitea.evolix.org/evolix/pve-storage.git), [pve-docs](https://gitea.evolix.org/evolix/pve-docs.git) et [pve-drbd](https://gitea.evolix.org/evolix/pve-drbd.git))
2. Compiler et installer avec `make dinstall` pve-docs, pve-drbd, pve-storage puis pve-manager (dans l'ordre donné). Des dependances vous seront demandés.

### Depuis le repo evolix

1. Ajouter la source au fichier '/etc/apt/sources.list.d/evolix_public.sources:
  ```
  Types: deb
  URIs: http://pub.evolix.org/evolix
  Suites: bookworm
  Components: main
  Signed-by: /etc/apt/keyrings/pub_evolix.as
  ```
2. Télécharger le clé public evolix:
  ```
  # wget https://pub.evolix.org/evolix/pub.asc -O /etc/apt/keyrings/pub_evolix.asc
  # chmod 644 /etc/apt/keyrings/pub_evolix.asc
  ```
3. Mettre à jour la liste des paquets:
  ```
  # apt update
  ```
4. Installer les nouvelles versions des paquets dans l'ordre:
  ```
  # apt upgrade
  ```
4.1 Ou installer les paquets individualements:
  ```
  # apt install pve-docs libpve-drbd-perl pve-storage pve-manager
  ```

## 5. Description détaillée des modifications et fonctionnement

Les fichiers Perl de Proxmox sont, après installation, stockés dans le dossier `/usr/share/perl5/PVE`.
Tous les paquets Proxmox contenant des appels API auront donc leurs fichiers Perl relatifs à l'API à partir du dossier PVE.

## pve-manager:

Le paquet `pve-manager` se décompose en deux parties :
- Interface utilisateur (UI) définie dans le dossier `www/`
- Base des appels API, définie dans le dossier `PVE/`

### Interface:

Le dossier `manager6` contient tous les fichiers JavaScript.

**Fonctionnement :**
- Le dossier `node` comporte toutes les fenêtres disponibles lors de la sélection d'un nœud.
- Le dossier `storage` comporte toutes les fenêtres de création de stockage.
- Le dossier `dc` comporte toutes les fenêtres disponibles lors de la sélection du Datacenter.

**Modifications :**
- `Utils.js` : Ajout du bouton de création du stockage DRBD et définition de son type (Datacenter -> storage -> add) ainsi que des messages pour les tâches.
- `storage/DRBDEdit.js` : Fenêtre de création du stockage avec ses attributs (la ressource liée, etc.)
  **NOTE :** L'URL de l'API lors de la création du stockage n'est pas explicitement écrite. Le `DRBDInputPanel` hérite de `PVE.panel.StorageBase` qui appelle l'API en lui envoyant le type (ici drbd) qui va rediriger les appels dans le paquet `pve-storage` (voir Fonctionnement `pve-storage`).
- `dc/DRBD*.js` : Toutes les fenêtres pour DRBD (gestion des fichiers, des ressources, etc.)
- `dc/Config.js` : Ajouts des fenêtres dans le menu déroulant.

### Appels API:

**Fonctionnement :**
- `API2.pm` est le point d'entrée pour les requêtes API. On peut y trouver toutes les redirections vers les différents modules Perl. La plupart d'entre eux se trouvent dans le dossier `API2/`.
- `CLI/` comporte plusieurs CLI pour la gestion de Proxmox (`pvenode`, `pvesh`, etc.). Leur fonctionnement réside dans de simples appels API lors de l'exécution de commandes.

**Modifications :**
- `API2.pm` : Ajout de la redirection vers le fichier `API2/Drbd.pm` lors des appels API `api2/json/drbd`.
- `API2/Drbd.pm` : Redirection de toutes les requêtes DRBD vers les bons modules Perl, ainsi que quelques appels 'utils'.

## pve-storage:

**Fonctionnement :**
- `Storage.pm` : Fichier central pour la gestion des types de stockage.
- `Storage/` : Contient les modules relatifs aux différents types de stockage.

**Modifications :**
- `Storage/DRBDPlugin.pm` : Nouveau fichier ajoutant le support du stockage DRBD.
- `Storage.pm` : Ajout du type de stockage DRBD dans la gestion centrale des types de stockage.

## pve-docs:

**Fonctionnement :**
- Script qui recupère tous les fichiers adoc présent pour les compiler.

**Modifications :**
- `pvedrbd.adoc` : Ajout de la documentation relative a DRBD, accessible depuis l'UI de promox.

## pve-drbd:

**Fonctionnement :**
- Nouveau paquet pour la gestion spécifique de DRBD. Le paquet contient tous les appels API ainsi que les fonction pour gerer les resources, fichiers et utils utils pour l'integration de DRBD

**Fichiers principaux :**
- `PVE/CLI/pve-drbd` : CLI pour la gestion de DRBD. Chaque commande effectue un appel API, traite la reponse (par exemple pour la formater) et l'écrit sur la sortie standart.
- `PVE/DRBD/` : Contient toutes les fonctions nécessaire à l'execution des taches.
- `PVE/API2/` : Contient toutes les methodes pour les appels API.

# Exemple d'utilisation

Une documentation est disponible dans Proxmox UI dans l'onglet DRBD, expliquant pas à pas les étapes à suivre pour creer votre propre VM drbd.