include /usr/share/dpkg/default.mk

DESTDIR=
PREFIX=/usr
PACKAGE=libpve-drbd-perl
BUILDDIR = $(PACKAGE)-$(DEB_VERSION_UPSTREAM)
DEB=$(PACKAGE)_$(DEB_VERSION)_$(DEB_HOST_ARCH).deb

export PERLDIR=$(PREFIX)/share/perl5

all:

.PHONY: install
install: PVE bin
	$(MAKE) -C bin install
	$(MAKE) -C PVE install

.PHONY: test
test:
	$(MAKE) -C test

.PHONY: clean
clean:
	$(MAKE) -C bin clean
	$(MAKE) -C PVE clean
	rm -f $(PACKAGE)*.tar* country.dat *.deb *.dsc *.build *.buildinfo *.changes
	rm -rf dest $(PACKAGE)-[0-9]*/

GITVERSION:=$(shell git rev-parse --short=16 HEAD)
$(BUILDDIR):
	rm -rf $@ $@.tmp
	mkdir $@.tmp
	rsync -a * $@.tmp
	mv $@.tmp $@

.PHONY: deb
deb: $(DEB)
$(DEB): $(BUILDDIR)
	cd $(BUILDDIR); dpkg-buildpackage -b -us -uc
	lintian $(DEB)

.PHONY: dinstall
dinstall: $(DEB)
	dpkg -i $(DEB)
